//
//  AppDelegate.h
//  G360Framework
//
//  Created by System Admin on 30/09/2016.
//  Copyright © 2016 System Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

