//
//  main.m
//  G360Framework
//
//  Created by System Admin on 30/09/2016.
//  Copyright © 2016 System Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
