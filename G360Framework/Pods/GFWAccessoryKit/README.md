# GFWAccessoryKit

[![CI Status](http://img.shields.io/travis/Maxime CHAPELET/GFWAccessoryKit.svg?style=flat)](https://travis-ci.org/Maxime CHAPELET/GFWAccessoryKit)
[![Version](https://img.shields.io/cocoapods/v/GFWAccessoryKit.svg?style=flat)](http://cocoapods.org/pods/GFWAccessoryKit)
[![License](https://img.shields.io/cocoapods/l/GFWAccessoryKit.svg?style=flat)](http://cocoapods.org/pods/GFWAccessoryKit)
[![Platform](https://img.shields.io/cocoapods/p/GFWAccessoryKit.svg?style=flat)](http://cocoapods.org/pods/GFWAccessoryKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GFWAccessoryKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GFWAccessoryKit"
```

## Author

Maxime CHAPELET, maxime.chapelet@giroptic.com

## License

GFWAccessoryKit is available under the MIT license. See the LICENSE file for more info.
