//
//  GFWAccessoryService.h
//  Pods
//
//  Created by Maxime CHAPELET on 21/06/2016.
//
//

#import <Foundation/Foundation.h>

@protocol GFWAccessory;
@protocol GFWAccessorySession;

@protocol GFWAccessoryService <NSObject>
@required

- (void)stop;

- (void)startWithAccessoryConnection:
            (void (^)(id<GFWAccessory> accessory))connectionBlock
                       disconnection:(void (^)(id<GFWAccessory> accessory))
                                         disconnectionBlock;

- (BOOL)accessory:(id<GFWAccessory>)accessory
    isSupportingProtocol:(NSString *)protocolString;

- (id<GFWAccessorySession>)accessory:(id<GFWAccessory>)accessory
              openSessionForProtocol:(NSString *)protocolString;

@end

@interface GFWAccessoryService : NSObject <GFWAccessoryService>

@end
