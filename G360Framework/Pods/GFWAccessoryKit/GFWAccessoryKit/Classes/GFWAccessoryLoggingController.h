//
//  GFWAccessoryLoggingController.h
//  Pods
//
//  Created by Maxime CHAPELET on 11/07/2016.
//
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

extern NSString *const kFileLoggerDidLogMessageNotification;

@interface GFWFileLogger : DDFileLogger
@end

@interface GFWAccessoryLoggingController : NSObject

+ (GFWFileLogger *)fileLogger;
@end
