//
// Created by Maxime CHAPELET on 21/06/2016.
//

#import <Foundation/Foundation.h>

@protocol GFWAccessory;


@protocol GFWAccessorySession <NSObject>
- (id <GFWAccessory>)accessory;

- (NSString *)protocolString;

- (NSInputStream *)inputStream;

- (NSOutputStream *)outputStream;
@end

@interface GFWAccessorySession : NSObject <GFWAccessorySession>

@end