//
// Created by Giroptic on 28/07/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFWAccessorySessionStreamThread.h"

static NSRunLoop *_gfwAccessorySessionStreamWriteRunLoop;
static NSThread *_gfwAccessorySessionStreamWriteThread;

static NSRunLoop *_gfwAccessorySessionStreamReadRunLoop;
static NSThread *_gfwAccessorySessionStreamReadThread;

dispatch_semaphore_t _streamReadThreadReadySemaphore;
dispatch_semaphore_t _streamWriteThreadReadySemaphore;

@implementation GFWAccessorySessionStreamThread {
}

+ (NSRunLoop *)readRunLoop {
  [self readThread];
  return _gfwAccessorySessionStreamReadRunLoop;
}

+ (NSRunLoop *)writeRunLoop {
  [self writeThread];
  return _gfwAccessorySessionStreamWriteRunLoop;
}


+ (void)readThread {
  static dispatch_once_t token;
  dispatch_once(&token, ^{
    _streamReadThreadReadySemaphore = dispatch_semaphore_create(0);
    _gfwAccessorySessionStreamReadThread =
      [[NSThread alloc] initWithTarget:self
                              selector:@selector(runLoopReadThread)
                                object:nil];
    [_gfwAccessorySessionStreamReadThread start];
    dispatch_semaphore_wait(_streamReadThreadReadySemaphore, DISPATCH_TIME_FOREVER);
  });
}

+ (void)writeThread {
  static dispatch_once_t token;
  dispatch_once(&token, ^{
    _streamWriteThreadReadySemaphore = dispatch_semaphore_create(0);
    _gfwAccessorySessionStreamWriteThread =
      [[NSThread alloc] initWithTarget:self
                              selector:@selector(runLoopWriteThread)
                                object:nil];
    [_gfwAccessorySessionStreamWriteThread start];
    dispatch_semaphore_wait(_streamWriteThreadReadySemaphore, DISPATCH_TIME_FOREVER);
  });
}

+ (void)runLoopReadThread {
  [NSThread currentThread].name = @"streamRunLoopReadThread";
  NSRunLoop *streamRunLoop = [NSRunLoop currentRunLoop];
  [streamRunLoop addPort:[NSMachPort port] forMode:NSDefaultRunLoopMode];
  _gfwAccessorySessionStreamReadRunLoop = streamRunLoop;
  dispatch_semaphore_signal(_streamReadThreadReadySemaphore);
  [streamRunLoop run];
}

+ (void)runLoopWriteThread {
  [NSThread currentThread].name = @"streamRunLoopWriteThread";
  NSRunLoop *streamRunLoop = [NSRunLoop currentRunLoop];
  [streamRunLoop addPort:[NSMachPort port] forMode:NSDefaultRunLoopMode];
  _gfwAccessorySessionStreamWriteRunLoop = streamRunLoop;
  dispatch_semaphore_signal(_streamWriteThreadReadySemaphore);
  [streamRunLoop run];
}

@end