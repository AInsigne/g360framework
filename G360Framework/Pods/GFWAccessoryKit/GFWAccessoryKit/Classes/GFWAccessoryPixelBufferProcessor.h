//
// Created by Giroptic on 08/09/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
@protocol GFWAccessoryPixelBufferProcessor <NSObject>

- (void)processPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end