//
// Created by Maxime CHAPELET on 21/06/2016.
//

#import <ExternalAccessory/ExternalAccessory.h>
#import "GFWAccessory.h"
#import "GFWAccessory+Internal.h"

@interface GFWAccessory ()
@property(nonatomic, strong, readwrite) EAAccessory *accessory;
@end

@implementation GFWAccessory {

}

- (BOOL)isConnected {
    return [self.accessory isConnected];
}

- (NSUInteger)connectionID {
    return [self.accessory connectionID];
}

- (NSString *)name {
    return [self.accessory name];
}

- (NSString *)manufacturer {
    return [self.accessory manufacturer];
}

- (NSString *)modelNumber {
    return [self.accessory modelNumber];
}

- (NSString *)serialNumber {
    return [self.accessory serialNumber];
}

- (NSString *)firmwareRevision {
    return [self.accessory firmwareRevision];
}

- (NSString *)hardwareRevision {
    return [self.accessory hardwareRevision];
}

- (NSArray <NSString *> *)protocolStrings {
    return [self.accessory protocolStrings];
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ",
                                                                     NSStringFromClass([self class])];
    [description appendString:self.accessory.description];
    [description appendString:@">"];
    return description;
}

@end

@implementation GFWAccessory (Internal)

- (instancetype)initWithExternalAccessory:(EAAccessory *)accessory {
    self = [super init];
    if (self) {
        self.accessory = accessory;
    }
    return self;
}

@end