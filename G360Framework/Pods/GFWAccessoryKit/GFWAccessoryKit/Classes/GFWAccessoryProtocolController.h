//
// Created by Giroptic on 19/07/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
@protocol GFWAccessorySessionController;

@protocol GFWAccessoryProtocolController <NSObject>
@property(nonatomic, weak) id<GFWAccessorySessionController> sessionController;
- (NSString *)protocolString;
- (void)readData:(NSMutableData *)data;
- (void)open;
- (void)close;
@end