//
//  GFWAccessoryDebugLogViewController.h
//  Pods
//
//  Created by Maxime CHAPELET on 11/07/2016.
//
//

#import <UIKit/UIKit.h>

@interface GFWAccessoryDebugLogViewController : UIViewController

+ (void)addToViewController:(UIViewController *)parentViewController;
@end
