//
// Created by Maxime CHAPELET on 21/06/2016.
//

#import "GFWAccessory+Internal.h"
#import "GFWAccessory.h"
#import "GFWAccessoryConstants.h"
#import "GFWAccessorySession+Internal.h"
#import "GFWAccessorySession.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <ExternalAccessory/ExternalAccessory.h>

@interface GFWAccessorySession ()
@property(nonatomic, strong) EASession *session;
@end

@implementation GFWAccessorySession {
  GFWAccessory *_accessory;
}

- (GFWAccessory *)accessory {
  return _accessory;
}

- (NSString *)protocolString {
  return [self.session protocolString];
}

- (NSInputStream *)inputStream {
  return [self.session inputStream];
}

- (NSOutputStream *)outputStream {
  return [self.session outputStream];
}

- (NSString *)description {
  NSMutableString *description = [NSMutableString
      stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
  if (self.session) {
    [description appendString:self.session.description];
  }
  [description appendString:@">"];
  return description;
}

@end

@implementation GFWAccessorySession (Internal)

- (instancetype)initWithAccessory:(GFWAccessory *)accessory
                      forProtocol:(NSString *)protocolString {
  self = [super init];
  if (self) {
    if (!accessory.accessory) {
      DDLogError(@"Internal accessory not available for accessory %@",
                 accessory);
      return nil;
    }
    self.session = [[EASession alloc] initWithAccessory:accessory.accessory
                                            forProtocol:protocolString];
    if (!self.session) {
      DDLogError(@"Can't create internal session for accessory %@",
                 accessory.accessory);
      return nil;
    }
    _accessory = accessory;
    DDLogVerbose(@"Created session %@ for accessory %@", self.session,
               _accessory);
  }
  return self;
}
@end