//
// Created by Giroptic on 28/07/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface GFWAccessorySessionStreamThread : NSObject

+ (NSRunLoop *)readRunLoop;
+ (NSRunLoop *)writeRunLoop;

@end