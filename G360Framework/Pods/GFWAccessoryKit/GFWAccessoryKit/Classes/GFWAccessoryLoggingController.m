//
//  GFWAccessoryLoggingController.m
//  Pods
//
//  Created by Maxime CHAPELET on 11/07/2016.
//
//

#import "GFWAccessoryLoggingController.h"

NSString *const kFileLoggerDidLogMessageNotification = @"kFileLoggerDidLogMessageNotification";

@implementation GFWFileLogger : DDFileLogger

- (void)logMessage:(DDLogMessage *)logMessage {
    [super logMessage:logMessage];

    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]
                               postNotificationName:kFileLoggerDidLogMessageNotification
                                             object:nil
                                           userInfo:nil];
    });
}

@end

@implementation GFWAccessoryLoggingController

+ (GFWFileLogger *)fileLogger {
    static GFWFileLogger *fileLogger;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        fileLogger = [[GFWFileLogger alloc] init];
    });
    return fileLogger;
}

@end
