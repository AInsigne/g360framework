//
// Created by Maxime CHAPELET on 21/06/2016.
//

#import <Foundation/Foundation.h>

@protocol GFWAccessorySession;

@protocol GFWAccessory <NSObject>
@required
- (BOOL)isConnected;

- (NSUInteger)connectionID;

- (NSString *)name;

- (NSString *)manufacturer;

- (NSString *)modelNumber;

- (NSString *)serialNumber;

- (NSString *)firmwareRevision;

- (NSString *)hardwareRevision;

- (NSArray <NSString *> *)protocolStrings;
@end

@class EAAccessory;

@interface GFWAccessory : NSObject <GFWAccessory>

@end