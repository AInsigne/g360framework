//
// Created by Maxime CHAPELET on 22/06/2016.
//

#import <Foundation/Foundation.h>
#import "GFWAccessory.h"

@interface GFWAccessory (Internal)
@property(nonatomic, strong, readonly) EAAccessory *accessory;

- (instancetype)initWithExternalAccessory:(EAAccessory *)accessory;
@end