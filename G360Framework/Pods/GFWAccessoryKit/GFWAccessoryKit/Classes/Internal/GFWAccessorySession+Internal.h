//
// Created by Maxime CHAPELET on 22/06/2016.
//

#import <Foundation/Foundation.h>
#import "GFWAccessorySession.h"

@class GFWAccessory;

@interface GFWAccessorySession (Internal)
- (instancetype)initWithAccessory:(GFWAccessory *)accessory
                      forProtocol:(NSString *)protocolString;
@end