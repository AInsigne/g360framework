//
// Created by Giroptic on 21/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <CoreMedia/CoreMedia.h>

@protocol GFWAccessorySampleBufferProcessor <NSObject>

 @required
- (void)start;
- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer
   formatDescriptionChanged:(BOOL)formatDescriptionChanged
                      isIDR:(BOOL)isIDR;
- (void)stop;

@end