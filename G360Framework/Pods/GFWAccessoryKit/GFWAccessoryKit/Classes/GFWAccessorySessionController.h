//
// Created by Maxime CHAPELET on 23/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GFWAccessoryProtocolController;
@protocol GFWAccessory;
@protocol GFWAccessoryService;

@protocol GFWAccessorySessionController <NSObject>
- (void)open;
- (void)close;
- (void)sendBytes:(uint8_t const *)data size:(NSUInteger)size;
@end

@interface GFWAccessorySessionController
    : NSObject <GFWAccessorySessionController>

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype) new NS_UNAVAILABLE;
- (instancetype)initWithAccessory:(id<GFWAccessory>)accessory
               protocolController:(id<GFWAccessoryProtocolController>)controller
                          service:(id<GFWAccessoryService>)service
    NS_DESIGNATED_INITIALIZER;
@end