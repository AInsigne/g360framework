//
//  GFWAccessoryDebugLogViewController.m
//  Pods
//
//  Created by Maxime CHAPELET on 11/07/2016.
//
//

#import "DDFileLogger.h"
#import "GFWAccessoryDebugLogViewController.h"
#import "GFWAccessoryLoggingController.h"

@interface GFWAccessoryDebugLogViewController ()
@property(weak, nonatomic) IBOutlet UIView *debugView;
@property(weak, nonatomic) IBOutlet UITextView *debugLogTextView;

@property(nonatomic) BOOL autoscrollEnabled;
@property(nonatomic) BOOL debugLogNeedsRefresh;
@property(nonatomic, strong) NSTimer *debugLogRefreshTimer;
@end

@implementation GFWAccessoryDebugLogViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.autoscrollEnabled = YES;
  [self showDebugView:NO];
  // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(didLogMessage:)
             name:kFileLoggerDidLogMessageNotification
           object:nil];
  self.debugLogRefreshTimer =
      [NSTimer scheduledTimerWithTimeInterval:0.5f
                                       target:self
                                     selector:@selector(refreshDebugLog)
                                     userInfo:nil
                                      repeats:YES];
}

- (void)didLogMessage:(NSNotification *)notification {
  self.debugLogNeedsRefresh = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:kFileLoggerDidLogMessageNotification
              object:nil];
  [self.debugLogRefreshTimer invalidate];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

+ (void)addToViewController:(UIViewController *)parentViewController {
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  GFWAccessoryDebugLogViewController *debug;
  debug = [[GFWAccessoryDebugLogViewController alloc]
      initWithNibName:@"GFWAccessoryDebugLogViewController"
               bundle:bundle];
  [debug willMoveToParentViewController:parentViewController];

  [parentViewController addChildViewController:debug];
  [debug didMoveToParentViewController:parentViewController];
  [parentViewController.view addSubview:debug.view];
  [self configureContstraintsWithParentViewController:parentViewController
                                  childViewController:debug];
  UIScreenEdgePanGestureRecognizer *gestureRecognizer;
  gestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc]
      initWithTarget:debug
              action:@selector(didSwipeFromRightEdge:)];
  gestureRecognizer.edges = UIRectEdgeRight;
  [parentViewController.view addGestureRecognizer:gestureRecognizer];
}

+ (void)configureContstraintsWithParentViewController:
            (UIViewController *)parentViewController
                                  childViewController:
                                      (UIViewController *)childViewController {
  childViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
  NSLayoutConstraint *constraint;
  constraint = [NSLayoutConstraint constraintWithItem:childViewController.view
                                            attribute:NSLayoutAttributeLeading
                                            relatedBy:NSLayoutRelationEqual
                                               toItem:parentViewController.view
                                            attribute:NSLayoutAttributeLeading
                                           multiplier:1.0f
                                             constant:0.0f];
  [parentViewController.view addConstraint:constraint];
  constraint = [NSLayoutConstraint constraintWithItem:childViewController.view
                                            attribute:NSLayoutAttributeTrailing
                                            relatedBy:NSLayoutRelationEqual
                                               toItem:parentViewController.view
                                            attribute:NSLayoutAttributeTrailing
                                           multiplier:1.0f
                                             constant:0.0f];
  [parentViewController.view addConstraint:constraint];
  constraint =
      [NSLayoutConstraint constraintWithItem:childViewController.view
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                      toItem:parentViewController.topLayoutGuide
                                   attribute:NSLayoutAttributeBottom
                                  multiplier:1.0f
                                    constant:0.0f];
  [parentViewController.view addConstraint:constraint];
  constraint = [NSLayoutConstraint
      constraintWithItem:childViewController.view
               attribute:NSLayoutAttributeBottom
               relatedBy:NSLayoutRelationEqual
                  toItem:parentViewController.bottomLayoutGuide
               attribute:NSLayoutAttributeTop
              multiplier:1.0f
                constant:0.0f];
  [parentViewController.view addConstraint:constraint];
}

- (void)didSwipeFromRightEdge:(id)sender {
  [self showDebugView:YES];
}

- (void)showDebugView:(BOOL)show {
  [self.debugView setHidden:!show];
  [self.view setUserInteractionEnabled:show];
  if (show) {
    self.debugLogNeedsRefresh = YES;
    [self refreshDebugLog];
  }
}

- (void)refreshDebugLog {
  if (self.debugView.isHidden || !self.debugLogNeedsRefresh) {
    return;
  }
  self.debugLogNeedsRefresh = NO;
  NSString *logContent = [self currentLogContent];
  self.debugLogTextView.text = logContent;
  [self scroll];
}

- (void)scroll {
  if (self.autoscrollEnabled) {
    NSRange range = NSMakeRange(self.debugLogTextView.text.length - 1, 1);
    [self.debugLogTextView scrollRangeToVisible:range];
  }
}

- (NSString *)currentLogContent {
  DDLogFileInfo *logFileInfos =
      [GFWAccessoryLoggingController fileLogger].currentLogFileInfo;
  NSString *logContent =
      [NSString stringWithContentsOfFile:logFileInfos.filePath
                                encoding:NSUTF8StringEncoding
                                   error:nil];
  return logContent;
}

- (IBAction)didSwipeDebugView:(id)sender {
  [self showDebugView:NO];
}

- (IBAction)clearButtonTouched:(id)sender {
  __weak typeof(self) weakSelf = self;
  [[GFWAccessoryLoggingController fileLogger] rollLogFileWithCompletionBlock:^{
    dispatch_async(dispatch_get_main_queue(), ^{
      weakSelf.debugLogNeedsRefresh = YES;
      [weakSelf refreshDebugLog];
    });
  }];
}

- (IBAction)sendButtonTouched:(id)sender {
  UIActivityViewController *activityView = [[UIActivityViewController alloc]
      initWithActivityItems:@[ self.currentLogContent ]
      applicationActivities:nil];
  [self presentViewController:activityView animated:NO completion:nil];
}

- (IBAction)crashButtonTouched:(id)sender {
  void *ptr;
  memset(ptr, 0, 256);
}

- (IBAction)autoscrollSwitchTouched:(id)sender {
  self.autoscrollEnabled = ((UISwitch *)sender).isOn;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
