//
// Created by Maxime CHAPELET on 23/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWAccessorySessionController.h"
#import "GFWAccessorySessionStreamThread.h"
#import <GFWAccessoryKit/GFWAccessoryKit.h>

NSUInteger const kMaxReadBufferSize = 512 << 10; // 512KB
NSUInteger const kMaxBuffersAllocationCount = 8;

@interface GFWAccessorySessionController () <NSStreamDelegate>
@property(nonatomic, strong) id<GFWAccessorySession> session;
@property(nonatomic, strong) NSMutableData *sendingDataBuffer;
@property(nonatomic, strong) id<GFWAccessoryProtocolController>
    protocolController;
@property(nonatomic, strong) dispatch_queue_t streamReadQueue;
@property(nonatomic, strong) dispatch_queue_t streamWriteQueue;
@property(nonatomic, strong) dispatch_queue_t streamPacketsIsolationQueue;
@property(nonatomic, strong) dispatch_queue_t bufferPoolQueue;
@property(nonatomic, strong) dispatch_queue_t sendingDataBufferQueue;
@property(nonatomic, strong) id<GFWAccessory> accessory;
@property(nonatomic, strong) id<GFWAccessoryService> service;
@property(nonatomic, strong) NSMutableArray *readBuffersPool;
@property(nonatomic, strong) NSMutableArray *readBuffersQueue;
@property(nonatomic) NSUInteger allocatedBuffers;
@property(nonatomic, strong) dispatch_semaphore_t streamPacketsWaitSem;
@end

@implementation GFWAccessorySessionController {
}

- (NSMutableData *)popBuffer {
  __block NSMutableData *buffer = nil;
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.bufferPoolQueue, ^{
    if (!weakSelf.readBuffersPool) {
      weakSelf.readBuffersPool = [NSMutableArray array];
    }
    if (weakSelf.readBuffersPool.count == 0) {
      buffer = [NSMutableData dataWithLength:kMaxReadBufferSize];
      weakSelf.allocatedBuffers++;
    } else {
      buffer = [weakSelf.readBuffersPool lastObject];
      buffer.length = kMaxReadBufferSize;
      [buffer resetBytesInRange:NSMakeRange(0, [buffer length])];
      [weakSelf.readBuffersPool removeLastObject];
    }
    DDLogVerbose(@"Buffers used : %llu",
               weakSelf.allocatedBuffers - weakSelf.readBuffersPool.count);
  });

  return buffer;
}

- (void)putBuffer:(NSMutableData *)buffer {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(weakSelf.bufferPoolQueue, ^{
    if (!weakSelf.readBuffersPool) {
      weakSelf.readBuffersPool = [NSMutableArray array];
    }
    [weakSelf.readBuffersPool addObject:buffer];
  });
}

- (id)initWithAccessory:(id<GFWAccessory>)accessory
     protocolController:(id<GFWAccessoryProtocolController>)controller
                service:(id<GFWAccessoryService>)service {
  self = [super init];
  if (self) {
    self.sendingDataBuffer = [NSMutableData dataWithCapacity:500];
    self.accessory = accessory;
    self.protocolController = controller;
    self.service = service;
    dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(
        DISPATCH_QUEUE_SERIAL, QOS_CLASS_DEFAULT, 0);
    self.streamReadQueue =
        dispatch_queue_create("gfwaccessory.streamRead", attr);
    self.streamWriteQueue =
        dispatch_queue_create("gfwaccessory.streamWrite", NULL);
    self.sendingDataBufferQueue =
        dispatch_queue_create("gfwaccessory.sendingDataBuffer", NULL);
    self.bufferPoolQueue =
        dispatch_queue_create("gfwaccessory.bufferPool", NULL);
    self.streamPacketsIsolationQueue =
        dispatch_queue_create("gfwaccessory.bufferWait", NULL);
  }
  return self;
}

- (BOOL)isOutputStreamOpen {
  return [GFWAccessorySessionController isStreamOpen:self.session.outputStream];
}

- (BOOL)isInputStreamOpen {
  return [GFWAccessorySessionController isStreamOpen:self.session.inputStream];
}

+ (BOOL)isStreamOpen:(NSStream *)stream {
  return stream.streamStatus != NSStreamStatusClosed &&
         stream.streamStatus != NSStreamStatusNotOpen &&
         stream.streamStatus != NSStreamStatusOpening;
}

- (void)open {
  if (self.session) {
    [self close];
  }
  DDLogVerbose(@"Opening session...");
  self.session =
      [self.service accessory:self.accessory
          openSessionForProtocol:self.protocolController.protocolString];
  DDLogVerbose(@"Opening streams...");
  [self openInputStream];
  [self openOutputStream];
  [self.protocolController open];
  self.readBuffersQueue = [NSMutableArray new];
}

- (void)close {
  if (!self.session) {
    return;
  }
  [self.protocolController close];
  DDLogVerbose(@"Closing streams...");
  [self closeInputStream];
  [self closeOutputStream];
  DDLogVerbose(@"Closing session...");
  self.session = nil;
}

- (void)openInputStream {
  if (self.session.inputStream) {
    [self logStreamStatus:self.session.inputStream];
    DDLogVerbose(@"Open input stream %@...", self.session.inputStream);
    [self openStream:self.session.inputStream
             runLoop:[GFWAccessorySessionStreamThread readRunLoop]];
    [self logStreamStatus:self.session.inputStream];
  }
}

- (void)openOutputStream {
  if (self.session.outputStream) {
    [self logStreamStatus:self.session.outputStream];
    DDLogVerbose(@"Open output stream %@...", self.session.outputStream);
    [self openStream:self.session.outputStream
             runLoop:[GFWAccessorySessionStreamThread writeRunLoop]];
    [self logStreamStatus:self.session.outputStream];
  }
}

- (void)closeInputStream {
  if (self.session.inputStream) {
    [self logStreamStatus:self.session.inputStream];
    DDLogVerbose(@"Close input stream %@...", self.session.inputStream);
    [self closeStream:self.session.inputStream
              runLoop:[GFWAccessorySessionStreamThread readRunLoop]];
    [self logStreamStatus:self.session.inputStream];
  }
}

- (void)closeOutputStream {
  if (self.session.outputStream) {
    [self logStreamStatus:self.session.outputStream];
    DDLogVerbose(@"Close output stream %@...", self.session.outputStream);
    [self closeStream:self.session.outputStream
              runLoop:[GFWAccessorySessionStreamThread writeRunLoop]];
    [self logStreamStatus:self.session.outputStream];
  }
}

- (void)openStream:(NSStream *)stream runLoop:(NSRunLoop *)runLoop {
  stream.delegate = self;
  [stream scheduleInRunLoop:runLoop forMode:NSDefaultRunLoopMode];
  [stream open];
}

- (void)closeStream:(NSStream *)stream runLoop:(NSRunLoop *)runLoop {
  [stream close];
  [stream removeFromRunLoop:runLoop forMode:NSDefaultRunLoopMode];
  stream.delegate = nil;
}

- (void)setProtocolController:
    (id<GFWAccessoryProtocolController>)protocolController {
  _protocolController = protocolController;
  protocolController.sessionController = self;
}

- (void)dealloc {
  [self close];
  DDLogVerbose(@"Dealloc session controller");
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
  switch (eventCode) {
  case NSStreamEventNone: {
    DDLogVerbose(@"Stream event none.");
    break;
  }
  case NSStreamEventOpenCompleted: {
    DDLogVerbose(@"Stream event has open completed.");
    break;
  }
  case NSStreamEventHasBytesAvailable: {
    DDLogVerbose(@"Stream event has bytes available.");
    [self read];
    break;
  }
  case NSStreamEventHasSpaceAvailable: {
    DDLogVerbose(@"Stream event has space available.");
    [self write];
    break;
  }
  case NSStreamEventErrorOccurred: {
    DDLogVerbose(@"Stream event error occurred.");
    break;
  }
  case NSStreamEventEndEncountered: {
    DDLogVerbose(@"Stream event end encountered.");
    break;
  }
  }
}

- (void)write {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(weakSelf.streamWriteQueue, ^{
    while ([weakSelf.sendingDataBuffer length] > 0 &&
           [weakSelf.session.outputStream hasSpaceAvailable] &&
           [weakSelf.session.outputStream streamStatus] == NSStreamStatusOpen) {
      DDLogVerbose(@"Will write to output stream %@", weakSelf.sendingDataBuffer);
      NSInteger bytesSent = [weakSelf.session.outputStream
              write:weakSelf.sendingDataBuffer.bytes
          maxLength:[weakSelf.sendingDataBuffer length]];
      if (bytesSent > 0) {
        DDLogVerbose(@"Output stream write success");
        [weakSelf popSendingBytes:(NSUInteger)bytesSent];
      } else if (bytesSent == -1) {
        DDLogError(@"Output stream write error");
      }
    }
  });
}
- (void)read {
  DDLogVerbose(@"Read stream.");
  __weak typeof(self) weakSelf = self;
  NSMutableData *packetData = [NSMutableData dataWithLength:kMaxReadBufferSize];
  NSInteger bytesRead = 0;
  bytesRead = [weakSelf.session.inputStream read:packetData.mutableBytes
                                       maxLength:kMaxReadBufferSize];
  if (bytesRead > 0) {
    if (bytesRead == kMaxReadBufferSize) {
      DDLogInfo(@"Bytes read equal max read buffer size of %u bytes", kMaxReadBufferSize);
    }
    packetData.length = (NSUInteger)bytesRead;
    [weakSelf.protocolController readData:packetData];
  }
}

- (void)popSendingBytes:(NSUInteger)count {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.sendingDataBufferQueue, ^{
    [weakSelf.sendingDataBuffer replaceBytesInRange:NSMakeRange(0, count)
                                          withBytes:NULL
                                             length:0];
  });
}

- (void)appendSendingBytes:(uint8_t *)data size:(NSUInteger)size {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.sendingDataBufferQueue, ^{
    [weakSelf.sendingDataBuffer appendBytes:data length:size];
  });
}

- (void)sendBytes:(uint8_t const *)data size:(NSUInteger)size {
  [self appendSendingBytes:data size:size];
  [self write];
}

- (void)logStreamStatus:(NSStream *)stream {
  switch ([stream streamStatus]) {
  case NSStreamStatusOpen: {
    DDLogVerbose(@"Stream %@ status : Open", stream);
    break;
  }
  case NSStreamStatusAtEnd: {
    DDLogVerbose(@"Stream %@ status : At End", stream);
    break;
  }
  case NSStreamStatusNotOpen: {
    DDLogVerbose(@"Stream %@ status : Not Open", stream);
    break;
  }
  case NSStreamStatusOpening: {
    DDLogVerbose(@"Stream %@ status : Opening", stream);
    break;
  }
  case NSStreamStatusReading: {
    DDLogVerbose(@"Stream %@ status : Reading", stream);
    break;
  }
  case NSStreamStatusWriting: {
    DDLogVerbose(@"Stream %@ status : Writing", stream);
    break;
  }
  case NSStreamStatusClosed: {
    DDLogVerbose(@"Stream %@ status : Closed", stream);
    break;
  }
  case NSStreamStatusError: {
    DDLogVerbose(@"Stream %@ status : Error", stream);
    break;
  }
  }
}
@end