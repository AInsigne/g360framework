//
//  GFWAccessoryConstants.h
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 18/07/2016.
//
//
#import <CocoaLumberjack/CocoaLumberjack.h>

#ifndef GFWAccessoryConstants_h
#define GFWAccessoryConstants_h

static const DDLogLevel ddLogLevel = DDLogLevelDebug;

#endif /* GFWAccessoryConstants_h */
