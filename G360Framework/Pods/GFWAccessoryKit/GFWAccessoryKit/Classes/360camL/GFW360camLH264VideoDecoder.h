//
// Created by Giroptic on 21/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFWAccessorySampleBufferProcessor.h"

@protocol GFWAccessoryPixelBufferProcessor;

@interface GFW360camLH264VideoDecoder: NSObject <GFWAccessorySampleBufferProcessor>

@property(nonatomic, strong) id<GFWAccessoryPixelBufferProcessor>
  pixelBufferProcessor;

@end