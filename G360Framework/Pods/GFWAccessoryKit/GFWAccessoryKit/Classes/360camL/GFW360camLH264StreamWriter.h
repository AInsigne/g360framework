//
// Created by Giroptic on 03/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GFW360camLStreamProcessor.h"


@interface GFW360camLH264StreamWriter: NSObject <GFW360camLStreamProcessor>
@end