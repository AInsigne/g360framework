//
// Created by Giroptic on 03/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLH264RawNALUsParser.h"
#import "GFW360camLH264ParametersSets.h"
#import "GFW360camLNALU.h"
#import "GFW360camLNALUToSampleBufferConverter.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>

NSString *const kH264ParametersSetDidChangeNotification =
    @"kH264ParametersSetDidChangeNotification";

/*Float64 const kPTSFreq = 1.0 / 90000.0;*/
uint32_t kNALUStartingCode4Value = 0x01000000;

@interface GFW360camLH264RawNALUsParser ()
//@property(nonatomic, strong) GFWClock *masterClock;
@property(atomic) BOOL started;
@property(nonatomic, strong, readwrite)
    NSMutableArray<id<GFW360camLNALUsProcessor>> *nalusProcessors;
@property(nonatomic) uint64_t previousTimestamp;
@property(nonatomic) uint64_t frame;
@property(nonatomic) BOOL newPPS;
@property(nonatomic) BOOL newSPS;
@property(nonatomic, strong) GFW360camLH264ParametersSets *h264ParameterSets;
@property(nonatomic) uint32_t gopSize;
@property(nonatomic) uint64_t accessUnitCount;
@end

@implementation GFW360camLH264RawNALUsParser {
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.nalusProcessors = [NSMutableArray new];
    self.h264ParameterSets = [GFW360camLH264ParametersSets new];
  }

  return self;
}

- (void)start {
  self.started = YES;
  self.frame = 0;
  self.previousTimestamp = 0;
  for (id<GFW360camLNALUsProcessor> nalusProcessor in self.nalusProcessors) {
    [nalusProcessor start];
  }
}

- (void)stop {
  self.started = NO;
  for (id<GFW360camLNALUsProcessor> nalusProcessor in self.nalusProcessors) {
    [nalusProcessor stop];
  }
}

- (void)processData:(uint8_t *)buf
               size:(uint32_t)size
          timestamp:(uint64_t)timestamp
             format:(video_format_t)format {
  if (format != VID_FORMAT_H264_RAW) {
    return;
  }
  if (!self.started) {
    return;
  }
  self.frame++;
  DDLogVerbose(@"Frame : %lu - Timestamp : %llu - Delay : %llu", self.frame,
               timestamp, timestamp - self.previousTimestamp);
  self.previousTimestamp = timestamp;
  NSArray *foundNALUs = [self findNALUs:buf size:size timestamp:timestamp];
  // TODO: Not sure we have to copy data, will see later to just use pointer ref
  [self copyNALUs:foundNALUs buffer:buf size:size];

  for (id<GFW360camLNALUsProcessor> naluProcessor in self.nalusProcessors) {
    [naluProcessor processNALUs:foundNALUs];
  }
}

- (void)copyNALUs:(NSArray *)nalus
           buffer:(const uint8_t *)buf
             size:(uint32_t)size {
  NSEnumerator *enumerator = [nalus objectEnumerator];
  GFW360camLNALU *element = enumerator.nextObject;
  while (element) {
    uint32_t naluDataOffset = element.offset;
    uint32_t naluDataEnd = size;
    GFW360camLNALU *next = enumerator.nextObject;
    if (next) {
      naluDataEnd = next.offset;
    }
    uint32_t dataSize = naluDataEnd - naluDataOffset;
    element.data = [[NSMutableData alloc] initWithBytes:(buf + naluDataOffset)
                                                 length:dataSize];
    [self findAndStoreParameterSetsWithNALU:element];
    element = next;
  }
}

- (void)findAndStoreParameterSetsWithNALU:(GFW360camLNALU *)nalu {
  if (nalu.type == 0x7) { // SPS NALU found
    self.h264ParameterSets.sps = [nalu.data copy];
    self.newSPS = YES;
  } else if (nalu.type == 0x8) { // PPS NALU found
    self.h264ParameterSets.pps = [nalu.data copy];
    self.newPPS = YES;
  }
  if (self.newPPS && self.newSPS) {
    self.newPPS = self.newSPS = NO;
    GFW360camLH264ParametersSets *set = [self.h264ParameterSets copy];
    if ([self.delegate
            respondsToSelector:@selector(h264ParameterSetsDidChange:)]) {
      [self.delegate h264ParameterSetsDidChange:set];
    }
    [[NSNotificationCenter defaultCenter]
        postNotificationName:kH264ParametersSetDidChangeNotification
                      object:set];
  }
}

- (NSArray *)findNALUs:(const uint8_t *)buf
                  size:(uint32_t)size
             timestamp:(uint64_t)timestamp {
  NSMutableArray *foundNALUs = [NSMutableArray new];
  for (uint32_t i = 0; i < size; i++) {
    uint32_t value = *((uint32_t *)(buf + i));
    if (value == kNALUStartingCode4Value) {
      uint8_t type = (*(buf + i + 4)) & ((uint8_t)0x1F);
      DDLogVerbose(@"Found NALU of type %@ (offset:%u cts:%llu)",
                   naluTypesStrings[type], i, timestamp);
      if(type == 5 && self.accessUnitCount > 0) {
        DDLogVerbose(@"I-Frame, GOP size : %u", self.gopSize);
        /*if (self.gopSize > 30) {
          uint32_t gopSize = self.gopSize;
          dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *view = [[UIAlertView alloc]
                    initWithTitle:@"GOP"
                          message:[NSString stringWithFormat:@"GOP is %d",
                                            gopSize]
                         delegate:nil
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil];
            [view show];
          });
        }*/
        self.gopSize = 0;
      } else if (type == 1){
        DDLogVerbose(@"P-Frame");
        self.gopSize ++;
      } else if (type == 9){
        self.accessUnitCount ++;
      }
      GFW360camLNALU *nalu = [GFW360camLNALU new];
      nalu.offset = i;
      nalu.type = type;
      nalu.timestamp = timestamp;
      [foundNALUs addObject:nalu];
    }
  }
  return foundNALUs;
}

- (void)requestCurrentH264ParameterSets:
    (void (^)(GFW360camLH264ParametersSets *))response {
  if (response) {
    response([self.h264ParameterSets copy]);
  }
}

- (GFW360camLH264ParametersSets *)currentH264ParameterSets {
  return [self.h264ParameterSets copy];
}


@end