//
// Created by Giroptic on 10/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLH264RawNALUsParser.h"
#import "GFW360camLH264VideoDecoder.h"
#import "GFW360camLStreamProcessor.h"
#import <Foundation/Foundation.h>

@protocol GFW360camLMP4WriterDelegate <NSObject>
- (void)didWriteSampleAtTime:(CMTime)cmTime;
- (void)didFinishWriteFileAtURL:(NSURL*)url;
@end

@interface GFW360camLMP4Writer : NSObject <GFWAccessorySampleBufferProcessor>
@property(nonatomic, weak) id<GFW360camLMP4WriterDelegate> delegate;
- (void)writeWithFilePath:(NSString *)path;
- (void)writeWithFileURL:(NSURL *)url;
- (void)finish:(void (^)())completion __deprecated_msg("Use finish instead and implement didFinishWriteFileAtURL: delegate method.");
- (void)finish;
- (BOOL)isWriting;
@end