//
// Created by Giroptic on 21/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>

@class GFW360camLNALU;

@protocol GFW360camLNALUsProcessor <NSObject>

@required
- (void)start;
- (void)processNALUs:(NSArray<GFW360camLNALU *> *)nalus;
- (void)stop;
@end