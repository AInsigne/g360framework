//
// Created by Giroptic on 03/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLStreamProcessor.h"
@class GFW360camLH264RawNALUsParser;
@class GFW360camLNALU;

@protocol GFW360camLNALUsProcessor;
@class GFW360camLH264ParametersSets;

extern NSString *const kH264ParametersSetDidChangeNotification;

@protocol GFW360camLH264RawNALUsParserDelegate <NSObject>
- (void)h264ParameterSetsDidChange:
    (GFW360camLH264ParametersSets *)h264ParameterSets;
@end

@interface GFW360camLH264RawNALUsParser : NSObject <GFW360camLStreamProcessor>

@property(nonatomic, strong, readonly)
    NSMutableArray<id<GFW360camLNALUsProcessor>> *nalusProcessors;

@property(nonatomic, weak) id<GFW360camLH264RawNALUsParserDelegate> delegate;

- (void)requestCurrentH264ParameterSets:
    (void (^)(GFW360camLH264ParametersSets *))response;

- (GFW360camLH264ParametersSets *)currentH264ParameterSets;
@end