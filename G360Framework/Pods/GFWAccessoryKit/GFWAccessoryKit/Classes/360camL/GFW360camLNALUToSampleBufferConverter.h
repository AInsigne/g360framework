//
// Created by Giroptic on 20/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLNALUsProcessor.h"

@class GFW360camLNALU;
@protocol GFWAccessorySampleBufferProcessor;

@interface GFW360camLNALUToSampleBufferConverter
    : NSObject <GFW360camLNALUsProcessor>

@property(nonatomic, strong, readonly)
    NSMutableArray<id<GFWAccessorySampleBufferProcessor>>
        *sampleBufferProcessors;

@end