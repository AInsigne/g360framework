//
// Created by Giroptic on 20/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLNALUToSampleBufferConverter.h"
#import "GFW360camLH264VideoDecoder.h"
#import "GFW360camLNALU.h"
#import <CocoaLumberjack/DDLogMacros.h>
#import <GFWAccessoryKit/GFWAccessoryConstants.h>

Float64 const kPTSFreq = 1.0 / 90000.0;

@interface GFW360camLNALUToSampleBufferConverter ()
@property(nonatomic, strong) GFW360camLNALU *sps;
@property(nonatomic, strong) GFW360camLNALU *pps;
@property(nonatomic, strong, readwrite)
    NSMutableArray<id<GFWAccessorySampleBufferProcessor>>
        *sampleBufferProcessors;
@end

@implementation GFW360camLNALUToSampleBufferConverter {

  CMFormatDescriptionRef _videoFormatDescription;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.sampleBufferProcessors = [NSMutableArray new];
  }

  return self;
}

- (void)start {
  [self clearVideoFormatDescription];
  for (id<GFWAccessorySampleBufferProcessor> sampleBufferProcessor in self
           .sampleBufferProcessors) {
    [sampleBufferProcessor start];
  }
  self.sps = nil;
  self.pps = nil;
}

- (void)processNALUs:(NSArray<GFW360camLNALU *> *)nalus {
  [self convert:nalus];
}

- (void)stop {
  for (id<GFWAccessorySampleBufferProcessor> sampleBufferProcessor in self
           .sampleBufferProcessors) {
    [sampleBufferProcessor stop];
  }
}

- (void)convert:(NSArray<GFW360camLNALU *> *)nalus {

  if (nalus.count == 0) {
    return;
  }

  DDLogVerbose(@"Will decode %d NALUs", nalus.count);

  NSUInteger decodedFrames = 0;

  // Proceed nalus

  NSUInteger i = 0;
  for (i = 0; i < nalus.count; i++) {
    GFW360camLNALU *nalu = nalus[i];
    GFW360camLNALU *frame = nil;
    BOOL isIDR = NO;
    if (nalu.type == 0x7) { // SPS NALU found
      [self clearVideoFormatDescription];
      /*[self clearDecompSession];*/
      self.sps = nalu;
    } else if (nalu.type == 0x8) { // PPS NALU found
      [self clearVideoFormatDescription];
      /*[self clearDecompSession];*/
      self.pps = nalu;
    } else if (nalu.type == 0x1) { // non-IDR
      // Replace start code with NALU length
      uint32_t *dataPtr = (uint32_t *)nalu.data.mutableBytes;
      *dataPtr = OSSwapInt32(nalu.data.length - 4);
      frame = nalu;
    } else if (nalu.type == 0x5) { // IDR
      // Replace start code with NALU length
      isIDR = YES;
      uint32_t *dataPtr = (uint32_t *)nalu.data.mutableBytes;
      *dataPtr = OSSwapInt32(nalu.data.length - 4);
      frame = nalu;
    }

    OSStatus status = noErr;
    BOOL didCreateNewFormatDescription = NO;
    if ([self shouldCreateNewVideoFormatDescription]) {
      status = [self createVideoFormatDescription];
      if (status == noErr) {
        didCreateNewFormatDescription = YES;
      }
    }

    if (!frame || !_videoFormatDescription) {
      continue;
    }

    CMBlockBufferRef blockBuffer = NULL;
    CMSampleBufferRef sampleBuffer = NULL;

    status = [self createBlockBuffer:&blockBuffer frame:frame];

    if (status == noErr) {
      status = [self createSampleBuffer:&sampleBuffer
                                  frame:frame
                            blockBuffer:blockBuffer];
      CFRelease(blockBuffer);
    }

    if (status == noErr) {
      DDLogVerbose(@"Render sample buffer.");
      CFArrayRef attachments =
          CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
      CFMutableDictionaryRef dict =
          (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
      CFDictionarySetValue(dict, kCMSampleAttachmentKey_DisplayImmediately,
                           kCFBooleanFalse);

      for (id<GFWAccessorySampleBufferProcessor> sampleBufferProcessor in self
               .sampleBufferProcessors) {
        [sampleBufferProcessor processSampleBuffer:sampleBuffer
                          formatDescriptionChanged:didCreateNewFormatDescription
                                             isIDR:isIDR];
      }

      decodedFrames++;
      CFRelease(sampleBuffer);
    } else {
      DDLogVerbose(@"Can't render sample buffer.");
    }
  }
}

- (OSStatus)createSampleBuffer:(CMSampleBufferRef *)sampleBuffer
                         frame:(GFW360camLNALU *)frame
                   blockBuffer:(CMBlockBufferRef)blockBuffer {
  size_t sampleSize;
  sampleSize = frame.data.length;
  CMSampleTimingInfo timingInfo;
  Float64 pts_seconds = frame.timestamp * kPTSFreq;
  CMTime cmTime = CMTimeMakeWithSeconds(pts_seconds, 90000);
  timingInfo.presentationTimeStamp = cmTime;
  timingInfo.decodeTimeStamp = kCMTimeInvalid;
  const CMSampleTimingInfo times[1] = {timingInfo};
  OSStatus status = CMSampleBufferCreate(
      CFAllocatorGetDefault(), blockBuffer, true, NULL, NULL,
      _videoFormatDescription, 1, 1, times, 1, &sampleSize, sampleBuffer);
  return status;
}

- (OSStatus)createBlockBuffer:(CMBlockBufferRef *)blockBuffer
                        frame:(GFW360camLNALU *)frame {
  CMBlockBufferRef tmp = NULL;
  OSStatus status = CMBlockBufferCreateWithMemoryBlock(
      NULL, (void *)frame.data.bytes, frame.data.length, kCFAllocatorNull, NULL,
      0, frame.data.length, 0, &tmp);
  if (status != noErr)
    return status;
  status = CMBlockBufferCreateContiguous(
      NULL, tmp, NULL, NULL, 0, frame.data.length,
      kCMBlockBufferAlwaysCopyDataFlag | kCMBlockBufferAssureMemoryNowFlag,
      blockBuffer);
  CFRelease(tmp);
  return status;
}

- (OSStatus)createVideoFormatDescription {
  OSStatus status;

  uint8_t const *pointers[2] = {self.sps.data.bytes + 4,
                                self.pps.data.bytes + 4};
  size_t sizes[2] = {self.sps.data.length - 4, self.pps.data.length - 4};
  status = CMVideoFormatDescriptionCreateFromH264ParameterSets(
      CFAllocatorGetDefault(), 2, pointers, sizes, 4, &_videoFormatDescription);

  return status;
}

- (BOOL)shouldCreateNewVideoFormatDescription {
  return !_videoFormatDescription && self.sps && self.pps;
}

- (void)clearVideoFormatDescription {
  if (_videoFormatDescription != NULL) {
    CFRelease(_videoFormatDescription);
    _videoFormatDescription = NULL;
  }
}

@end