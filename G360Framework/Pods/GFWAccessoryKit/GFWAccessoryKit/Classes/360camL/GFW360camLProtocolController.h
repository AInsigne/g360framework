//
//  GFW360camLProtocolController.h
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 01/08/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>
@class GFW360camLH264RawNALUsParser;
@protocol GFW360camLStreamProcessor;

@protocol GFW360camLProtocolControllerDelegate <NSObject>
- (void)didWriteQboxPacketAtTime:(CMTime)cmTime;
@end

@interface GFW360camLProtocolController
    : NSObject <GFWAccessoryProtocolController>
@property(nonatomic, copy, readonly) NSString *protocolString;
@property(nonatomic, assign) BOOL dump;
@property(nonatomic, weak) id<GFW360camLProtocolControllerDelegate> delegate;

- (void)sendTestPacket1;

/*
 * This API is used to start the capture of video data from the specified
 * channel in the camera.
 */
- (void)startVideo;

/*
 * This API is used to stop the capture of video data from a channel in the
 * camera.
 */
- (void)stopVideo;

/*
 * This is used to force an I frame in the specified video channel. Applicable
 * only for VID_FORMAT_H264_RAW, VID_FORMAT_H264_TS, VID_FORMAT_H264_AAC_TS.
 */
- (void)forceIDR;

/*
 * This is used to set bitrate on the video channel specified. Typical Range:
 * 100000 (100 kbps) to 2000000 (2 Mbps)
 */
- (void)setBitrate:(uint32_t)bitrate;

- (id)initWithProtocol:(NSString *)protocolString
       streamProcessor:(id<GFW360camLStreamProcessor>)videoDecoder;
@end
