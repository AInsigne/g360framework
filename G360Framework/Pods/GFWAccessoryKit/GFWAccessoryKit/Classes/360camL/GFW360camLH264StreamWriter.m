//
// Created by Giroptic on 03/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLH264StreamWriter.h"

@interface GFW360camLH264StreamWriter ()
@property(nonatomic, strong) NSFileHandle *fileHandle;
@end
@implementation GFW360camLH264StreamWriter {
}
- (void)start {
  NSString *fileName =
      [[[NSUUID UUID] UUIDString] stringByAppendingPathExtension:@"h264"];
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                       NSUserDomainMask, YES);
  NSString *documentsPath = paths[0];
  NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
  if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
  }
  [[NSFileManager defaultManager] createFileAtPath:filePath
                                          contents:nil
                                        attributes:nil];
  self.fileHandle = [NSFileHandle
      fileHandleForWritingAtPath:[documentsPath
                                     stringByAppendingPathComponent:fileName]];
}
- (void)stop {
  [self.fileHandle closeFile];
}
- (void)processData:(uint8_t *)buf
               size:(uint32_t)size
          timestamp:(uint64_t)timestamp
             format:(video_format_t)format {
  [self.fileHandle
      writeData:[NSData dataWithBytesNoCopy:buf length:size freeWhenDone:NO]];
}

@end