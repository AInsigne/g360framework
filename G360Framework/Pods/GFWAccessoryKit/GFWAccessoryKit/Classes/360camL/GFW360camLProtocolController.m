//
//  GFW360camLProtocolController.m
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 01/08/2016.
//  Copyright 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFW360camLProtocolController.h"

#define BE_32(x)                                                               \
  (((uint32_t)(((uint8_t *)(x))[0]) << 24) | (((uint8_t *)(x))[1] << 16) |     \
   (((uint8_t *)(x))[2] << 8) | ((uint8_t *)(x))[3])

#define FOURCC(a, b, c, d)                                                     \
  ((uint32_t)(unsigned char)(d) << 24 | ((uint32_t)(unsigned char)(c) << 16) | \
   ((uint32_t)(unsigned char)(b) << 8) | ((uint32_t)(unsigned char)(a)))

#define TYPE_QBOX FOURCC('q', 'b', 'o', 'x')
#define TYPE_EOSP FOURCC('e', 'o', 's', 'p')

typedef struct {
  uint32_t size;
  uint32_t type;
} QboxPacketHeader;

typedef enum {
  IOS_ACC_UPGRADE_COMMANDS = 0x1,
  IOS_ACC_UPGRADE_RESPONSE,

  IOS_ACC_START_STREAM,
  IOS_ACC_STOP_STREAM,

  IOS_ACC_VIDEO_COMMANDS,
  IOS_ACC_AUDIO_COMMANDS,
  IOS_ACC_DEWARP_COMMANDS,
  IOS_ACC_ISP_COMMANDS,
  IOS_ACC_SYSTEM_COMMANDS

} ios_cmd_types_t;

typedef enum { VIDEO_CHANNEL = 0x1, AUDIO_CHANNEL = 0x2 } channel_type_t;

typedef struct {
  ios_cmd_types_t hdr;
  int mux_channel_id;
  channel_type_t type;
} ios_stream_state_cmd_t;

typedef enum {
  IOS_ACC_SET_BITRATE = 0x1,
  IOS_ACC_GET_BITRATE,

  IOS_ACC_SET_FORCE_IFRAME,

  IOS_ACC_SET_AVC_MAXNAL,
  IOS_ACC_GET_AVC_MAXNAL

} video_cmd_types_t;

typedef struct {
  ios_cmd_types_t hdr;
  video_cmd_types_t vcmd;
  int mux_channel_id;
  uint32_t data_length;
  uint32_t cmd_data;
} __attribute__((__packed__)) ios_video_cmd_t;

@interface GFW360camLProtocolController ()
@property(nonatomic, copy, readwrite) NSString *protocolString;
@property(nonatomic, strong) NSFileHandle *fileHandle;
@property(nonatomic, strong) id<GFW360camLStreamProcessor> streamProcessor;
@property(nonatomic, strong) GFW360camLProtocolPacket *currentPacket;
@property(atomic) BOOL isOpen;
@property(nonatomic) uint64_t previousTS;
@property(nonatomic) CFTimeInterval startDumpTime;
@property(nonatomic, strong) dispatch_queue_t isolationQueue;
@property(nonatomic, strong) dispatch_queue_t packetReadQueue;
@property(nonatomic, strong) NSMutableArray *packetsQueue;
@property(nonatomic, strong) dispatch_semaphore_t readSemaphore;
@property(nonatomic, strong) GFW360camLProtocolPacket *debugFinalizedPackets;
@property(nonatomic) BOOL debugMultiQboxPackets;
@end

@implementation GFW360camLProtocolController {
}

@synthesize sessionController;

- (void)sendTestPacket1 {
  [self forceIDR];
}

- (void)startVideo {
  ios_stream_state_cmd_t cmd;

  cmd.hdr = IOS_ACC_START_STREAM;
  cmd.mux_channel_id = 0;
  cmd.type = VIDEO_CHANNEL;

  NSData *packet = [NSData dataWithBytes:(uint8_t *)&cmd
                                  length:sizeof(ios_stream_state_cmd_t)];
  [self.sessionController sendBytes:packet.bytes size:packet.length];
}

- (void)stopVideo {
  ios_stream_state_cmd_t cmd;

  cmd.hdr = IOS_ACC_STOP_STREAM;
  cmd.mux_channel_id = 0;
  cmd.type = VIDEO_CHANNEL;

  NSData *packet = [NSData dataWithBytes:(uint8_t *)&cmd
                                  length:sizeof(ios_stream_state_cmd_t)];
  [self.sessionController sendBytes:packet.bytes size:packet.length];
}

- (void)forceIDR {
  ios_video_cmd_t cmd;

  cmd.hdr = IOS_ACC_VIDEO_COMMANDS;
  cmd.vcmd = IOS_ACC_SET_FORCE_IFRAME;
  cmd.mux_channel_id = 0;
  cmd.data_length = 0;
  cmd.cmd_data = 0;

  NSData *packet =
      [NSData dataWithBytes:(uint8_t *)&cmd length:sizeof(ios_video_cmd_t)];
  [self.sessionController sendBytes:packet.bytes size:packet.length];
}

- (void)setBitrate:(uint32_t)bitrate {
  /*
    bitrate = MAX(100000,bitrate);
    bitrate = MIN(2000000,bitrate);
  */
  ios_video_cmd_t cmd;

  cmd.hdr = IOS_ACC_VIDEO_COMMANDS;
  cmd.vcmd = IOS_ACC_SET_BITRATE;
  cmd.mux_channel_id = 0;
  cmd.data_length = sizeof(uint32_t);
  cmd.cmd_data = bitrate;

  NSData *packet =
      [NSData dataWithBytes:(uint8_t *)&cmd length:sizeof(ios_video_cmd_t)];
  [self.sessionController sendBytes:packet.bytes size:packet.length];
}

- (id)initWithProtocol:(NSString *)protocolString
       streamProcessor:(id<GFW360camLStreamProcessor>)videoDecoder {
  self = [super init];
  if (self) {
    self.protocolString = protocolString;
    self.streamProcessor = videoDecoder;
    self.isolationQueue = dispatch_queue_create("isolationQueue", NULL);
    self.packetReadQueue = dispatch_queue_create("packetReadQueue", NULL);
    self.packetsQueue = [NSMutableArray new];
  }
  return self;
}

- (void)readData:(NSMutableData *)data {
  CFTimeInterval startTime = CACurrentMediaTime();

  DDLogVerbose(@"Read %d bytes", (unsigned int)data.length);

  /*
    GFW360camLProtocolPacket *packet = [self createPacketWithData:data];
    if (packet) {
      [self queuePacket:packet];
      dispatch_semaphore_signal(self.readSemaphore);
      [self dumpToFile:packet];
    }
  */

  NSArray *packets = [self createPacketsWithData:data];
  for (GFW360camLProtocolPacket *packet in packets) {
    [self queuePacket:packet];
    dispatch_semaphore_signal(self.readSemaphore);
    [self dumpToFile:packet];
  }

  CFTimeInterval endTime = CACurrentMediaTime();
  CFTimeInterval delay = endTime - startTime;
  // If execution time is too long, camera might drop frames.
  // It might affect NALUs sequence and decoding, so we request a new I-frame.
  if (delay > 0.033) {
    DDLogWarn(@"Execution time %g", delay);
    [self forceIDR];
  }
}

- (void)queuePacket:(GFW360camLProtocolPacket *)packet {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.isolationQueue, ^{
    [weakSelf.packetsQueue addObject:packet];
  });
}

- (void)dumpToFile:(GFW360camLProtocolPacket *)packet {
  [self setupDumpFileHandle];
  if (self.dump && self.fileHandle) {
    if (self.startDumpTime == 0) {
      self.startDumpTime = CACurrentMediaTime();
    }
    CFTimeInterval currentTime = CACurrentMediaTime();
    if ([self.delegate
            respondsToSelector:@selector(didWriteQboxPacketAtTime:)]) {
      [self.delegate
          didWriteQboxPacketAtTime:CMTimeMakeWithSeconds(currentTime -
                                                             self.startDumpTime,
                                                         90000)];
    }
    DDLogVerbose(@"Dump time : %g", currentTime - self.startDumpTime);
  }
  [self.fileHandle writeData:packet.data];
}

- (void)open {
  self.isOpen = YES;
  [self.streamProcessor start];
  [self.packetsQueue removeAllObjects];
  self.readSemaphore = dispatch_semaphore_create(0);
  __weak typeof(self) weakSelf = self;
  dispatch_async(self.packetReadQueue, ^{
    [weakSelf readPacketsQueue];
  });
}

- (void)readPacketsQueue {
  while (self.isOpen) {
    dispatch_semaphore_wait(self.readSemaphore, DISPATCH_TIME_FOREVER);
    @autoreleasepool {
      NSArray *packets = [self dequeuePackets];
      if (packets.count > 1) {
        DDLogVerbose(@"Processing %u packets", packets.count);
      }
      for (GFW360camLProtocolPacket *packet in packets) {
        [self processPacket:packet];
      }
    }
  }
}

- (NSArray *)dequeuePackets {
  NSMutableArray *packets = [NSMutableArray new];
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.isolationQueue, ^{
    [packets addObjectsFromArray:weakSelf.packetsQueue];
    [weakSelf.packetsQueue removeAllObjects];
  });
  return packets;
}

- (void)close {
  [self.packetsQueue removeAllObjects];
  [self.streamProcessor stop];
  self.isOpen = NO;
  dispatch_semaphore_signal(self.readSemaphore);
  self.dump = NO;
}

- (NSString *)getDocumentsDirectoryPath {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                       NSUserDomainMask, YES);
  return paths[0];
}

- (void)setDump:(BOOL)dump {
  _dump = dump;
  if (!dump) {
    self.startDumpTime = 0;
    [self.fileHandle closeFile];
    self.fileHandle = nil;
  }
}

- (void)setupDumpFileHandle {
  if (!self.dump || self.fileHandle)
    return;

  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
  dateFormatter.dateFormat = @"yyyy-MM-dd--HH-mm-ss";
  dateFormatter.timeZone = [NSTimeZone localTimeZone];

  NSString *date = [dateFormatter stringFromDate:[NSDate date]];

  NSString *fileName =
      [[[[NSUUID UUID] UUIDString] stringByAppendingFormat:@"-%@", date]
          stringByAppendingPathExtension:@"qbox"];
  NSString *filePath = [[self getDocumentsDirectoryPath]
      stringByAppendingPathComponent:fileName];
  if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
  }
  [[NSFileManager defaultManager] createFileAtPath:filePath
                                          contents:nil
                                        attributes:nil];
  self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
}

- (void)processPacket:(GFW360camLProtocolPacket *)packet {
  if (packet.type == TYPE_QBOX) {
    DDLogVerbose(@"Will process QBox packet...");
    int channelID = 0;
    video_format_t fmt = FIRST_VID_FORMAT;
    uint8_t *data_buf = NULL;
    uint32_t size = 0;
    uint64_t timeStamp = 0;
    uint32_t analytics = 0;
    metadata_t metadata;
    qbox_parse_header((uint8_t *)packet.data.bytes, &channelID, &fmt, &data_buf,
                      &size, &timeStamp, &analytics, &metadata);
    self.previousTS = timeStamp;
    [self.streamProcessor processData:data_buf
                                 size:size
                            timestamp:timeStamp
                               format:fmt];
  }
}

- (NSArray *)createPacketsWithData:(NSData *)data {
  NSUInteger remainingDataToCopySize = data.length;
  QboxPacketHeader *header = NULL;
  uint64_t offset = 0;
  NSMutableArray *packets = [NSMutableArray new];
  while (remainingDataToCopySize > 0) {
    uint32_t size = 0;
    header = (QboxPacketHeader *)(data.bytes + offset);
    if (header->type == TYPE_QBOX) {
      if (self.debugFinalizedPackets && self.currentPacket) {
        DDLogDebug(@"Seems a packet has not been finalized");
        NSString *time = [NSString
            stringWithFormat:@"%llu",
                             (uint64_t)(CACurrentMediaTime() * 1000000)];
        NSString *filename = [time stringByAppendingPathExtension:@"part"];
        NSString *path = [[self getDocumentsDirectoryPath]
            stringByAppendingPathComponent:filename];
        [self.currentPacket.data writeToFile:path atomically:YES];
      }
      size = BE_32(&(header->size));
      DDLogVerbose(@"Found QBOX packet of size %u at offset %llu", size,
                   offset);
      self.currentPacket = [GFW360camLProtocolPacket new];
      self.currentPacket.type = TYPE_QBOX;
      self.currentPacket.data = [NSMutableData data];
      self.currentPacket.size = size;
      self.currentPacket.chunk = 0;
    }
    NSUInteger dataToFillSize =
        self.currentPacket.size - self.currentPacket.data.length;
    uint32_t dataToCopySize = MIN(dataToFillSize, remainingDataToCopySize);
    [self.currentPacket.data appendBytes:data.bytes + offset
                                  length:dataToCopySize];
    self.currentPacket.chunk++;
    if (self.currentPacket.data.length == self.currentPacket.size) {
      [packets addObject:self.currentPacket];
      self.currentPacket = nil;
    }
    remainingDataToCopySize = MAX(0, remainingDataToCopySize - dataToCopySize);
    offset += dataToCopySize;
    if (self.debugMultiQboxPackets && remainingDataToCopySize > 0) {
      DDLogDebug(@"Seems data contains more than one Qbox packet.");
      NSString *time = [NSString
          stringWithFormat:@"%llu", (uint64_t)(CACurrentMediaTime() * 1000000)];
      NSString *filename = [time stringByAppendingPathExtension:@"bin"];
      DDLogDebug(@"See offset %u in file %@", offset, filename);
      NSString *path = [[self getDocumentsDirectoryPath]
          stringByAppendingPathComponent:filename];
      [data writeToFile:path atomically:YES];
    }
  }
  return packets;
}

- (GFW360camLProtocolPacket *)createPacketWithData:(NSMutableData *)data {
  QboxPacketHeader *header = NULL;

  header = (QboxPacketHeader *)(data.bytes);
  GFW360camLProtocolPacket *finished = nil;
  uint32_t size = 1;
  BOOL newPacket = NO;
  if (header->type == TYPE_QBOX) {
    size = BE_32(&(header->size));
    DDLogVerbose(@"Found QBOX packet of size %d", size);
    self.currentPacket = [GFW360camLProtocolPacket new];
    self.currentPacket.type = TYPE_QBOX;
    self.currentPacket.data = [NSMutableData data];
    newPacket = YES;

  } else if (header->type == TYPE_EOSP) {
    size = BE_32(&(header->size));
    DDLogVerbose(@"Found End Of Stream packet of size %d", size);
    self.currentPacket = [GFW360camLProtocolPacket new];
    self.currentPacket.type = TYPE_EOSP;
    self.currentPacket.data = [NSMutableData data];
    newPacket = YES;
  }

  if (self.currentPacket && newPacket) {
    self.currentPacket.size = size;
    self.currentPacket.offset = 0;
    self.currentPacket.chunk = 0;
  }

  [self.currentPacket.data appendData:data];
  self.currentPacket.chunk++;
  if (self.currentPacket.data.length == self.currentPacket.size) {
    DDLogVerbose(@"Chunks : %u", self.currentPacket.chunk);
    finished = self.currentPacket;
    self.currentPacket = nil;
  }

  return finished;
}

@end
