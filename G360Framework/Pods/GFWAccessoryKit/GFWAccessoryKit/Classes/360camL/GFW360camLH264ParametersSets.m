//
//  GFW360camLH264ParametersSets.m
//  Pods
//
//  Created by Maxime CHAPELET on 15/09/2016.
//
//

#import "GFW360camLH264ParametersSets.h"

@implementation GFW360camLH264ParametersSets
- (id)copyWithZone:(NSZone *)zone {
  GFW360camLH264ParametersSets *set =
      [[GFW360camLH264ParametersSets allocWithZone:zone] init];
  set.pps = self.pps;
  set.sps = self.sps;
  return set;
}

@end