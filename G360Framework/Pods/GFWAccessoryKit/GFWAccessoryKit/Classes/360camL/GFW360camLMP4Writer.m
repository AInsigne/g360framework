//
// Created by Giroptic on 10/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLMP4Writer.h"
#import <AVFoundation/AVFoundation.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>

typedef NS_ENUM(NSUInteger, GFW360camLMP4WriterState) {
  MP4WriterStateWaiting = 0,
  MP4WriterStateWrite,
  MP4WriterStateFinish,
  MP4WriterStateFinishing,
  MP4WriterStateFinished
};

typedef NS_ENUM(NSUInteger, AssetWriterState) {
  AssetWriterStateNil = 0,
  AssetWriterStateStartFailed,
  AssetWriterStateCreated,
  AssetWriterStateWritingStarted,
  AssetWriterStateSessionStarted,
  AssetWriterStateFinishingSession,
  AssetWriterStateSessionFinished
};

@interface GFW360camLMP4Writer ()
@property(nonatomic) BOOL started;
@property(nonatomic, strong) AVAssetWriter *assetWriter;
@property(nonatomic, strong) AVAssetWriterInput *videoWriterInput;
@property(nonatomic) GFW360camLMP4WriterState mp4WriterState;
@property(nonatomic) AssetWriterState assetWriterState;
@property(nonatomic, strong) NSURL *fileURL;
@property(nonatomic, strong) dispatch_queue_t writeQueue;
@property(nonatomic, strong) dispatch_queue_t isolationQueue;
@property(nonatomic) CMTime timestamp;
@property(nonatomic) NSUInteger frameCount;
@property(nonatomic) BOOL gotFirstIDR;
@property(nonatomic, copy) void (^finishCompletionBlock)();
@property(nonatomic) CMTime previousTimeStamp;
@property(nonatomic) const CMTime writeTimeStamp;
@property(nonatomic) const CMTime timeStampDiff;
@end
@implementation GFW360camLMP4Writer {
  CFMutableArrayRef _videoSampleBuffers;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.isolationQueue = dispatch_queue_create("isolation", NULL);
  }

  return self;
}

- (void)start {
  self.started = YES;
  [self _finish:nil];
  self.writeQueue = dispatch_queue_create("myqueue", NULL);
  dispatch_async(self.writeQueue, ^{
    [self write];
  });
}

- (void)createAssetWriterIfNeeded {
  switch (self.assetWriterState) {
  case AssetWriterStateNil: {
    __block CMSampleBufferRef firstSampleBuffer = NULL;
    dispatch_sync(self.isolationQueue, ^{
      if (_videoSampleBuffers == NULL) {
        return;
      }
      CFIndex count = CFArrayGetCount(_videoSampleBuffers);
      if (count > 0) {
        firstSampleBuffer = CFArrayGetValueAtIndex(_videoSampleBuffers, 0);
      }
    });
    if (firstSampleBuffer == NULL) {
      return;
    }
    NSError *error = nil;
    self.assetWriter = [[AVAssetWriter alloc] initWithURL:self.fileURL
                                                 fileType:AVFileTypeMPEG4
                                                    error:&error];
    NSParameterAssert(self.assetWriter);

    CMFormatDescriptionRef formatDescription =
        CMSampleBufferGetFormatDescription(firstSampleBuffer);
    self.videoWriterInput =
        [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                           outputSettings:nil
                                         sourceFormatHint:formatDescription];

    self.videoWriterInput.expectsMediaDataInRealTime = YES;

    NSParameterAssert(self.videoWriterInput);
    NSParameterAssert([self.assetWriter canAddInput:self.videoWriterInput]);

    if ([self.assetWriter canAddInput:self.videoWriterInput]) {
      [self.assetWriter addInput:self.videoWriterInput];
    }
    self.assetWriterState = AssetWriterStateCreated;
    break;
  }
  case AssetWriterStateCreated: {
    if ([self.assetWriter startWriting]) {
      self.assetWriterState = AssetWriterStateWritingStarted;
    } else {
      self.assetWriterState = AssetWriterStateStartFailed;
    }
    break;
  }
  case AssetWriterStateWritingStarted: {
    __block CMTime startTime = kCMTimeZero;
    __block BOOL didReadStartTime = NO;
    dispatch_sync(self.isolationQueue, ^{
      if (_videoSampleBuffers == NULL) {
        return;
      }
      CFIndex count = CFArrayGetCount(_videoSampleBuffers);
      if (count > 0) {
        CMSampleBufferRef firstSampleBuffer =
            CFArrayGetValueAtIndex(_videoSampleBuffers, 0);
        startTime = CMSampleBufferGetPresentationTimeStamp(firstSampleBuffer);
        didReadStartTime = YES;
      }
    });
    if (didReadStartTime) {
      self.previousTimeStamp = startTime;
      self.writeTimeStamp = kCMTimeZero;
      self.timeStampDiff = kCMTimeZero;
      [self.assetWriter startSessionAtSourceTime:kCMTimeZero];
      self.assetWriterState = AssetWriterStateSessionStarted;
    }
    break;
  }
  case AssetWriterStateSessionStarted:
    break;
  case AssetWriterStateStartFailed:
    break;
  case AssetWriterStateFinishingSession:
    break;
  case AssetWriterStateSessionFinished:
    break;
  }
}

- (void)write {
  while (self.started) {
    switch (self.mp4WriterState) {
    case MP4WriterStateWaiting: {
      break;
    }
    case MP4WriterStateWrite: {
      [self writeSamples];
      break;
    }
    case MP4WriterStateFinish: {
      [self finishWriting];
      break;
    }
    case MP4WriterStateFinishing:
      break;
    case MP4WriterStateFinished:
      break;
    }
  }
}

- (void)writeSamples {
  [self createAssetWriterIfNeeded];
  if (self.assetWriterState != AssetWriterStateSessionStarted) {
    return;
  }
  [self writeVideoSamples];
}

- (void)writeVideoSamples {
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.isolationQueue, ^{
    CFIndex count = CFArrayGetCount(_videoSampleBuffers);
    for (int i = 0; i < count; i++) {
      if (!weakSelf.videoWriterInput.readyForMoreMediaData) {
        break;
      }
      CMSampleBufferRef buffer = CFArrayGetValueAtIndex(_videoSampleBuffers, i);

      //////
      CMTime timeStamp = CMSampleBufferGetPresentationTimeStamp(buffer);
      // prevent null or negative delay between frames
      if (CMTimeCompare(weakSelf.previousTimeStamp, timeStamp) < 0) {
        weakSelf.timeStampDiff =
            CMTimeSubtract(timeStamp, weakSelf.previousTimeStamp);
      }
      weakSelf.writeTimeStamp =
          CMTimeAdd(weakSelf.writeTimeStamp, weakSelf.timeStampDiff);
      CMTime previousTimeStamp = weakSelf.previousTimeStamp;
      weakSelf.previousTimeStamp = timeStamp;

      // set new sample timestamp
      CMSampleTimingInfo timingInfo;
      timingInfo.presentationTimeStamp = weakSelf.writeTimeStamp;
      timingInfo.decodeTimeStamp = kCMTimeInvalid;
      timingInfo.duration = kCMTimeIndefinite;
      const CMSampleTimingInfo times[1] = {timingInfo};
      CMSampleBufferRef bufferToWrite = NULL;
      OSStatus status = CMSampleBufferCreateCopyWithNewTiming(
          CFAllocatorGetDefault(), buffer, 1, times, &bufferToWrite);

      //////
      BOOL success =
          [weakSelf.videoWriterInput appendSampleBuffer:bufferToWrite];
      CFRelease(bufferToWrite);
      DDLogVerbose(@"success %@", success ? @"YES" : @"NO");
      DDLogVerbose(@"buffer ts : %f - diff ts:%f - write ts:%f",
                   CMTimeGetSeconds(timeStamp),
                   CMTimeGetSeconds(self.timeStampDiff),
                   CMTimeGetSeconds(self.writeTimeStamp));
      if (!success) {
        DDLogVerbose(@"%@", weakSelf.assetWriter.error);
        break;
      } else {
        if ([weakSelf.delegate
                respondsToSelector:@selector(didWriteSampleAtTime:)]) {
          dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.delegate didWriteSampleAtTime:weakSelf.writeTimeStamp];
          });
        }
        weakSelf.frameCount++;
        CFArrayRemoveValueAtIndex(_videoSampleBuffers, i);
        i--;
        count--;
      }
    }
  });
}

- (void)finishWriting {
  if (self.mp4WriterState == MP4WriterStateFinishing) {
    return;
  }
  self.assetWriterState = AssetWriterStateFinishingSession;
  self.mp4WriterState = MP4WriterStateFinishing;
  [self.videoWriterInput markAsFinished];
  __weak typeof(self) weakSelf = self;
  [self.assetWriter finishWritingWithCompletionHandler:^{
    weakSelf.mp4WriterState = MP4WriterStateFinished;
    weakSelf.assetWriterState = AssetWriterStateSessionFinished;
    if([weakSelf.delegate respondsToSelector:@selector(didFinishWriteFileAtURL:)]){
      [weakSelf.delegate didFinishWriteFileAtURL:weakSelf.fileURL];
    }
    weakSelf.mp4WriterState = MP4WriterStateWaiting;
    weakSelf.assetWriterState = AssetWriterStateNil;
    if (weakSelf.finishCompletionBlock) {
      weakSelf.finishCompletionBlock();
      weakSelf.finishCompletionBlock = nil;
    }
  }];
  dispatch_sync(self.isolationQueue, ^{
    if (_videoSampleBuffers != NULL) {
      CFRelease(_videoSampleBuffers);
      _videoSampleBuffers = NULL;
    }
  });
  self.videoWriterInput = nil;
  self.assetWriter = nil;
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer
    formatDescriptionChanged:(BOOL)formatDescriptionChanged
                       isIDR:(BOOL)isIDR {
  if (!self.started || self.mp4WriterState != MP4WriterStateWrite) {
    return;
  }
  if (isIDR) {
    self.gotFirstIDR = YES;
  }
  if (!self.gotFirstIDR) {
    return;
  }
  CMFormatDescriptionRef formatDescription =
      CMSampleBufferGetFormatDescription(sampleBuffer);
  CMMediaType mediaType = CMFormatDescriptionGetMediaType(formatDescription);
  switch (mediaType) {
  case kCMMediaType_Video: {
    [self copyVideoSampleBuffer:sampleBuffer];
    break;
  }
  case kCMMediaType_Audio: {
    break;
  }
  default:
    break;
  }
}

- (void)copyVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer {
  __block CMSampleBufferRef copy = NULL;
  OSStatus status =
      CMSampleBufferCreateCopy(CFAllocatorGetDefault(), sampleBuffer, &copy);
  if (status == noErr) {
    dispatch_sync(self.isolationQueue, ^{
      if (_videoSampleBuffers == NULL) {
        _videoSampleBuffers = CFArrayCreateMutable(kCFAllocatorDefault, 150,
                                                   &kCFTypeArrayCallBacks);
      }
      CFArrayAppendValue(_videoSampleBuffers, copy);
    });
  }
}

- (void)stop {
  __weak typeof(self)weakSelf = self;
  [self _finish:^{
    weakSelf.started = NO;
  }];
}

- (void)writeWithFilePath:(NSString *)path {
  [self writeWithFileURL:[NSURL fileURLWithPath:path]];
}

- (void)writeWithFileURL:(NSURL *)url {
  [self _finish:^{
    self.fileURL = url;
    self.gotFirstIDR = NO;
    self.mp4WriterState = MP4WriterStateWrite;
  }];
}

- (void)_finish:(void (^)())completion {
  if (self.mp4WriterState == MP4WriterStateWrite) {
    self.mp4WriterState = MP4WriterStateFinish;
    self.finishCompletionBlock = completion;
  } else {
    if (completion) {
      completion();
    }
  }
}

- (void)finish:(void (^)())completion {
  [self _finish:completion];
}

- (void)finish {
  [self _finish:nil];
}

- (BOOL)isWriting {
  return self.assetWriterState != MP4WriterStateWaiting;
}


@end