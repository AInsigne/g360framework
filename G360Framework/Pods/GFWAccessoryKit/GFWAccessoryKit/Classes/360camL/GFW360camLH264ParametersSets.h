//
//  GFW360camLH264ParametersSets.h
//  Pods
//
//  Created by Maxime CHAPELET on 15/09/2016.
//
//

#import <Foundation/Foundation.h>

@interface GFW360camLH264ParametersSets : NSObject <NSCopying>
@property (nonatomic, strong) NSData *pps;
@property (nonatomic, strong) NSData *sps;
@end
