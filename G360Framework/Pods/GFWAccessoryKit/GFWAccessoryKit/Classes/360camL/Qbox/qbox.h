/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef _QBOX_H
#define _QBOX_H

#include <stdint.h>

//typedef unsigned char uint8_t;
//typedef unsigned int uint32_t;
//typedef unsigned long long uint64_t;

/** enum of video format used in a particular channel */
typedef enum {
    FIRST_VID_FORMAT       = 0,
    /** H264 in elementary stream format */
    VID_FORMAT_H264_RAW    = 0,
    /** H264 in transport stream format */
    VID_FORMAT_H264_TS     = 1,
    /** MJPEG in elmentary stream format */
    VID_FORMAT_MJPEG_RAW   = 2,
    /** YUV stream in YUY2 format */
    VID_FORMAT_YUY2_RAW    = 3,
    /** YUV stream in NV12 format */
    VID_FORMAT_NV12_RAW    = 4,
    /** Luma stream format */
    VID_FORMAT_GREY_RAW    = 5,
    /** H264 and AAC in transport stream format */
    VID_FORMAT_H264_AAC_TS = 6,
    /** mux data */
    VID_FORMAT_MUX         = 7,
    /** datas */
    VID_FORMAT_METADATAS       = 8,
    /** mux data */
    VID_FORMAT_AAC_RAW		   = 9,
    /** total number of video formats supported */
    NUM_VID_FORMAT
} video_format_t;

typedef struct {
  /** TBD */
    char *qmedExt;
   /** TBD */
    unsigned int total_len;
} metadata_t;

/** enum to indicate the format type supported */
typedef enum {
    /** PCM format */
    AUD_FORMAT_PCM_RAW  = 0,
    /** AAC format */
    AUD_FORMAT_AAC_RAW  = 1,
    /** OPUS format */
    AUD_FORMAT_OPUS_RAW = 2,
    /** number of audio formats supported */
    NUM_AUD_FORMAT
} audio_format_t;

typedef struct
{
    /** audio timestamp in terms of ticks of 90khz clock where
    each tick corresponds to 1/(90 * 1000) sec or 1/90 ms */
    long long timestamp;
    /** size of the audio frame received */
    int framesize;
    /** sampling frequency at which the audio frame is captured */
    int samplefreq;
    /** number of audio channels captured by the microphone and/or
    encoded */
    int channelno;
    /** audio object type with which the Audio stream is encoded.
     This is useful to construct the ADTS Header in case of AAC
     encoded stream. Ignore in case of PCM or other format. */
    int audioobjtype;
    /** format of the captured audio */
    audio_format_t format;
    /** pointer to audio frame data */
    unsigned char *dataptr;
} audio_params_t;

typedef struct qbox_header {
  uint32_t box_size;
  uint32_t box_type;
  /* TODO test on BE and LE machines */
  struct {
    uint32_t version : 8;
    uint32_t flags   : 24;
  } box_flags;
  uint16_t stream_type;
  uint16_t stream_id;
  uint32_t sample_flags;
  uint32_t sample_cts;
  uint32_t sample_cts_low;
}__attribute__((__packed__)) qbox_header_t;

int qbox_parse_header(uint8_t *buf, int *channel_id, video_format_t *fmt,
                      uint8_t **data_buf, uint32_t *size, uint64_t *ts,
                      uint32_t *analytics, metadata_t *metadata);
int audio_param_parser(audio_params_t *h, unsigned char *buf, int len);
int get_qbox_hdr_size(void);
#endif	/* _QBOX_H */
