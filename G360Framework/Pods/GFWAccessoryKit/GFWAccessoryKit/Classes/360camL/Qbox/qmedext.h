/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: qmedext.h 50967 2014-11-18 01:31:55Z nmasood $
*******************************************************************************/

#ifndef __QMEDEXT_HH
#define __QMEDEXT_HH

#define QMED_FLAGS_DEWARP      0x01
#define QMED_FLAGS_SENSOR      0x02

//Types
typedef enum {
    QMED_EXT_TYPE_DEWARP = 0x01,
    QMED_EXT_TYPE_SENSOR = 0x02,
} qmed_ext_type_t;

#ifdef _WIN32
#define LITTLE_ENDIAN 0
#define BIG_ENDIAN    1
#define __BYTE_ORDER LITTLE_ENDIAN
#else
//#include <endian.h>
#endif

#define BE8(a) (a)
#if __BYTE_ORDER == BIG_ENDIAN
#define BE16(a) (a)
#define BE24(a) (a)
#define BE32(a) (a)
#define BE64(a) (a)
#else
#define BE16(a)                                                             \
    ((((a)>>8)&0xFF) |                                                      \
    (((a)<<8)&0xFF00))
#define BE24(a)                                                             \
    ((((a)>>16)&0xFF) |                                                     \
    ((a)&0xFF00) |                                                          \
    (((a)<<16)&0xFF0000))
#define BE32(a)                                                             \
    ((((a)>>24)&0xFF) |                                                     \
    (((a)>>8)&0xFF00) |                                                     \
    (((a)<<8)&0xFF0000) |                                                   \
    ((((a)<<24))&0xFF000000))
#define BE64(a)                                                             \
    (BE32(((a)>>32)&0xFFFFFFFF) |                                           \
    ((BE32((a)&0xFFFFFFFF)<<32)&0xFFFFFFFF00000000))
#endif

//Each extension consists of following header followed by specific data
typedef struct
{
    unsigned long boxSize;
    unsigned long boxType;
} QMedExtStruct;

//Following structures contain values in native endianness of codec
typedef struct
{
    QMedExtStruct hdr;
    int mapType;     //Of type EPTZ_MAP_TYPES as defined in codec/lib/dewarp/ePTZ_eWARP.h
    short divisor;
    short panel;
    int params[22];
} QMedExtDewarpStruct;

typedef struct
{
    QMedExtStruct hdr;
    unsigned int vts;
    unsigned int hts;
    unsigned int exposure;
    unsigned int analogGain_q24_8;
    unsigned int unclippedEV_q24_8;
} QMedExtSensorStruct;


/* Metadata macro (for host) */
#define METADATA_GET(metadata, data_ptr, data_struct_type, qmed_type) \
    do { \
        unsigned int cur_offset = 0; \
        data_ptr = NULL; \
        while(cur_offset < metadata->total_len) { \
            QMedExtStruct *extension = \
                (QMedExtStruct*)(metadata->qmedExt+cur_offset); \
            unsigned long size = BE32(extension->boxSize); \
            unsigned long type = BE32(extension->boxType); \
            cur_offset += size; \
            \
            if(type != qmed_type) { \
                continue; \
            } else { \
                data_ptr = (data_struct_type*) extension; \
                break; \
            } \
        } \
    } while(0)

#endif  /* __QMEDEXT_HH */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/
