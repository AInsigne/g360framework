//
// Created by Giroptic on 03/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GFW360camLQBox.h"

@protocol GFW360camLStreamProcessor <NSObject>
@required
- (void)start;
- (void)stop;
- (void)processData:(uint8_t *)buf
               size:(uint32_t)size
          timestamp:(uint64_t)timestamp
             format:(video_format_t)format;
@end