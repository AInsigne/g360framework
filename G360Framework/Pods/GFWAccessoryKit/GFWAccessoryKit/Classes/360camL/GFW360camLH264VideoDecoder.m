//
// Created by Giroptic on 21/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360camLH264VideoDecoder.h"
#import "GFW360camLPixelBufferQueueElement.h"
#import <CocoaLumberjack/DDLogMacros.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>
#import <VideoToolbox/VideoToolbox.h>

typedef struct {
  Float64 timer;
  Float64 previous_pts;
  Float64 previous_delay;
  Float64 previous_master_time;
} VideoState;

typedef NS_ENUM(NSUInteger, UpdateState) {
  UpdateStateNone = 0,
  UpdateStateDisplay,
  UpdateStateSkip
};

@implementation GFW360camLH264VideoDecoder {
  VTDecompressionSessionRef _decompressionSession;
  VideoState _videoState;
}

- (void)start {
  //  self.masterClock = nil;
  _videoState.previous_delay = 40e-3;
  _videoState.previous_pts = 0;
  _videoState.timer = 0;
  _videoState.previous_master_time = 0;
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer
   formatDescriptionChanged:(BOOL)formatDescriptionChanged
                      isIDR:(BOOL)isIDR {
  if (!sampleBuffer) {
    return;
  }
  OSStatus status = noErr;
  if (formatDescriptionChanged || _decompressionSession == NULL) {
    CMVideoFormatDescriptionRef formatDescription =
        CMSampleBufferGetFormatDescription(sampleBuffer);
    [self clearDecompressionSession];
    status = [self createDecompressionSession:formatDescription];
  }
  if (status == noErr) {
    [self render:sampleBuffer];
  }
}

- (void)stop {
  [self clearDecompressionSession];
}

- (void)render:(CMSampleBufferRef)sampleBuffer {
  if (_decompressionSession == NULL) {
    return;
  }
  VTDecodeFrameFlags flags = kVTDecodeFrame_EnableAsynchronousDecompression;
  VTDecodeInfoFlags flagOut;
  NSDate *currentTime = [NSDate date];
  VTDecompressionSessionDecodeFrame(_decompressionSession, sampleBuffer, flags,
                                    (__bridge_retained void *)currentTime,
                                    &flagOut);
}

- (BOOL)output:(GFW360camLPixelBufferQueueElement *)element {
  /*if (!self.masterClock) {
    self.masterClock = [GFWClock new];
    [self.masterClock start];
  }

  CMTime masterTime;
  [self.masterClock getTime:&masterTime];
  Float64 master_time_sec = CMTimeGetSeconds(masterTime);

  Float64 pts_seconds = CMTimeGetSeconds(element.pts);

  Float64 frame_delay = pts_seconds - _videoState.previous_pts;

  if (frame_delay <= 0 || frame_delay >= 1.0) {
    frame_delay = _videoState.previous_delay;
  }
  DDLogVerbose(@"Frame delay %f, PTS %f", frame_delay, pts_seconds);
  _videoState.previous_delay = frame_delay;
  _videoState.previous_pts = pts_seconds;

  _videoState.timer += frame_delay;

  Float64 timer_delay = _videoState.timer - master_time_sec;
  if (timer_delay < 0) {
    _videoState.timer -= timer_delay - 0.01;
  }
  UpdateState state;
  if (timer_delay < 0.01) {
    state = UpdateStateSkip;
    DDLogVerbose(@"Frame skip %f", timer_delay);
    timer_delay = 0.01;
  } else {
    DDLogVerbose(@"Display Delay %f", timer_delay);
    state = UpdateStateDisplay;
  }
  if (element.status != noErr) {
    timer_delay = 0.01;
    state = UpdateStateSkip;
  }*/

  if ([self.pixelBufferProcessor
          respondsToSelector:@selector(processPixelBuffer:)]) {
    __block CVPixelBufferRef pixelBuffer = element.pixelBuffer;
    CVPixelBufferRetain(pixelBuffer);
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.pixelBufferProcessor processPixelBuffer:pixelBuffer];
      CVPixelBufferRelease(pixelBuffer);
    });
  }

  // self.displayedFrames++;
  return YES;
}

void decompressionSessionDecodeFrameCallback(
    void *CM_NULLABLE decompressionOutputRefCon,
    void *CM_NULLABLE sourceFrameRefCon, OSStatus status,
    VTDecodeInfoFlags infoFlags, CM_NULLABLE CVImageBufferRef imageBuffer,
    CMTime presentationTimeStamp, CMTime presentationDuration) {

  NSDate *date = (__bridge_transfer NSDate *)sourceFrameRefCon;

  GFW360camLH264VideoDecoder *decoder =
      (__bridge GFW360camLH264VideoDecoder *)decompressionOutputRefCon;

  GFW360camLPixelBufferQueueElement *element;
  element = [GFW360camLPixelBufferQueueElement new];
  element.pixelBuffer = imageBuffer;
  element.pts = presentationTimeStamp;
  element.status = status;
  if (status != noErr) {
    DDLogError(@"Decoding Error");
  }
  DDLogVerbose(@"Enqueue decoded frame.");
  [decoder output:element];
}

- (OSStatus)createDecompressionSession:
    (CMVideoFormatDescriptionRef)videoFormatDescription {
  // make sure to destroy the old VTD session
  if (_decompressionSession != NULL) {
    return noErr;
  }
  VTDecompressionOutputCallbackRecord callBackRecord;
  callBackRecord.decompressionOutputCallback =
      decompressionSessionDecodeFrameCallback;

  // this is necessary if you need to make calls to Objective C "self" from
  // within in the callback method.
  callBackRecord.decompressionOutputRefCon = (__bridge void *)self;

  // you can set some desired attributes for the destination pixel buffer.  I
  // didn't use this but you may
  // if you need to set some attributes, be sure to uncomment the dictionary in
  // VTDecompressionSessionCreate
  NSDictionary *destinationImageBufferAttributes =
      @{(id)kCVPixelBufferOpenGLESCompatibilityKey : @YES };

  OSStatus status = VTDecompressionSessionCreate(
      NULL, videoFormatDescription, NULL,
      NULL, // (__bridge CFDictionaryRef)(destinationImageBufferAttributes)
      &callBackRecord, &_decompressionSession);
  DDLogVerbose(@"Video Decompression Session Create: \t %@",
               (status == noErr) ? @"successful!" : @"failed...");
  if (status != noErr)
    DDLogError(@"\t\t VTD ERROR type: %d", (int)status);
  return status;
}

- (void)clearDecompressionSession {
  if (_decompressionSession != NULL) {
    VTDecompressionSessionInvalidate(_decompressionSession);
    CFRelease(_decompressionSession);
    _decompressionSession = NULL;
  }
}

@end