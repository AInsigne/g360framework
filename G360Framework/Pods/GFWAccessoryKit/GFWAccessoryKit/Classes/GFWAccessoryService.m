//
//  GFWAccessoryService.m
//  Pods
//
//  Created by Maxime CHAPELET on 21/06/2016.
//
//
#import "GFWAccessoryService.h"
#import "GFWAccessory+Internal.h"
#import "GFWAccessory.h"
#import "GFWAccessoryConstants.h"
#import "GFWAccessoryLoggingController.h"
#import "GFWAccessorySession+Internal.h"
#import "GFWAccessorySession.h"
#import <ExternalAccessory/ExternalAccessory.h>

NSString *const kGFWAccessoryDidConnectNotification =
    @"kGFWAccessoryDidConnectNotification";
NSString *const kGFWAccessoryDidDisconnectNotification =
    @"kGFWAccessoryDidDisconnectNotification";

@interface GFWAccessoryService ()
@property(nonatomic, copy) void (^connectionBlock)(id<GFWAccessory>);
@property(nonatomic, copy) void (^disconnectionBlock)(id<GFWAccessory>);
@end

@implementation GFWAccessoryService {
}

- (void)stop {
  DDLogVerbose(@"Stopping Accessory Service.");
  [self removeObservers];
  [self unregisterManager];
  self.connectionBlock = nil;
  self.disconnectionBlock = nil;
  [DDLog removeLogger:[GFWAccessoryLoggingController fileLogger]];
}

- (void)startWithAccessoryConnection:
            (void (^)(id<GFWAccessory> accessory))connectionBlock
                       disconnection:(void (^)(id<GFWAccessory> accessory))
                                         disconnectionBlock {
  [DDLog addLogger:[GFWAccessoryLoggingController fileLogger]];
  DDLogVerbose(@"Starting Accessory Service.");
  self.connectionBlock = connectionBlock;
  self.disconnectionBlock = disconnectionBlock;
  [self checkConnectedAccessories];
  [self registerManager];
  [self addObservers];
}

- (void)checkConnectedAccessories {
  NSArray *accessories;
  accessories =
      [EAAccessoryManager sharedAccessoryManager].connectedAccessories;
  if (accessories.count > 0) {
    for (EAAccessory *accessory in accessories) {
      DDLogVerbose(@"Found connected accessory : %@", accessory);
      GFWAccessory *gfwAccessory =
          [[GFWAccessory alloc] initWithExternalAccessory:accessory];
      if (self.connectionBlock) {
        self.connectionBlock(gfwAccessory);
      }
    }
  }
}

- (BOOL)accessory:(id<GFWAccessory>)accessory
    isSupportingProtocol:(NSString *)protocolString {
  NSUInteger result = [accessory.protocolStrings
      indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [obj isEqualToString:protocolString];
        ;
      }];
  BOOL isSupporting = result != NSNotFound;
  DDLogVerbose(@"Accessory %@ the %@ protocol",
             isSupporting ? @"supports" : @"can't support", protocolString);
  return isSupporting;
}

- (GFWAccessorySession *)accessory:(GFWAccessory *)accessory
            openSessionForProtocol:(NSString *)protocolString {
  if ([self accessory:accessory isSupportingProtocol:protocolString]) {
    GFWAccessorySession *session;
    session = [[GFWAccessorySession alloc] initWithAccessory:accessory
                                                 forProtocol:protocolString];
    if (session) {
      DDLogVerbose(@"Session created : %@", session);
    }
    return session;
  }
  return nil;
}

- (void)registerManager {
  [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
}

- (void)unregisterManager {
  [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
}

- (void)addObservers {
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(accessoryDidConnect:)
             name:EAAccessoryDidConnectNotification
           object:nil];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(accessoryDidDisconnect:)
             name:EAAccessoryDidDisconnectNotification
           object:nil];
}

- (void)removeObservers {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)accessoryDidConnect:(NSNotification *)accessoryDidConnect {
  EAAccessory *accessory = [accessoryDidConnect userInfo][EAAccessoryKey];
  GFWAccessory *gfwAccessory =
      [[GFWAccessory alloc] initWithExternalAccessory:accessory];
  DDLogVerbose(@"Accessory did connect : %@", accessory);
  if (self.connectionBlock) {
    self.connectionBlock(gfwAccessory);
  }
}

- (void)accessoryDidDisconnect:(NSNotification *)accessoryDidDisconnect {
  EAAccessory *accessory = [accessoryDidDisconnect userInfo][EAAccessoryKey];
  GFWAccessory *gfwAccessory =
      [[GFWAccessory alloc] initWithExternalAccessory:accessory];
  DDLogVerbose(@"Accessory did disconnect : %@", accessory);
  if (self.disconnectionBlock) {
    self.disconnectionBlock(gfwAccessory);
  }
}

- (void)dealloc {
  [self stop];
}

@end
