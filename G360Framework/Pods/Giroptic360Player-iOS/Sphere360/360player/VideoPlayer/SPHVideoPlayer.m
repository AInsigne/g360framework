/**
 360° Player
 Copyright (c) 2016 Giroptic

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHVideoPlayer.h"

static const NSString *ItemStatusContext;

@interface SPHVideoPlayer ()<AVPlayerItemOutputPullDelegate>

@property(strong, nonatomic) AVPlayer *assetPlayer;
@property(strong, nonatomic) AVPlayerItem *playerItem;
@property(strong, nonatomic) AVURLAsset *urlAsset;
@property(strong, atomic) AVPlayerItemVideoOutput *videoOutput;

@property(nonatomic) BOOL isAudioEnabled;
@property(assign, nonatomic, readwrite) CGFloat assetDuration;
@end

@implementation SPHVideoPlayer

#pragma mark - LifeCycle

- (instancetype)initVideoPlayerWithURL:(NSURL *)urlAsset {
  if (self = [super init]) {
    self.isAudioEnabled = YES;
    [self initialSetupWithURL:urlAsset];
  }
  return self;
}

#pragma mark - Public

- (void)play {
  if ((self.assetPlayer.currentItem) &&
    (self.assetPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay)) {
    [self.assetPlayer play];
  }
}

- (void)pause {
  [self.assetPlayer pause];
}

- (void)seekPositionAtProgress:(CGFloat)progressValue
             withPlayingStatus:(BOOL)isPlaying {
  [self.assetPlayer pause];

  double time = self.assetDuration * progressValue;
  [self.assetPlayer seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
  if (isPlaying) {
    [self.assetPlayer play];
  }
}

- (void)setPlayerVolume:(CGFloat)volume __unused {
  self.assetPlayer.volume = (float) (volume > .0 ? MIN(volume, 0.7) : 0.0f);
}

- (void)setPlayerRate:(CGFloat)rate __unused {
  self.assetPlayer.rate = rate > .0 ? rate : 0.0f;
}

- (void)stop {
  [self.assetPlayer seekToTime:kCMTimeZero];
  self.assetPlayer.rate = .0f;
}

#pragma mark - Private

- (void)initialSetupWithURL:(NSURL *)url {
  NSDictionary *assetOptions = @{
    AVURLAssetPreferPreciseDurationAndTimingKey : @YES
  };
  self.urlAsset = [AVURLAsset URLAssetWithURL:url options:assetOptions];
  [self configVideoOutput];
}

- (void)prepareToPlay {
  NSArray *keys = @[@"tracks"];
  __weak SPHVideoPlayer *weakSelf = self;
  [self.urlAsset loadValuesAsynchronouslyForKeys:keys
                               completionHandler:^{
                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                                              1 * NSEC_PER_SEC),
                                                dispatch_get_main_queue(), ^{
                                     [weakSelf startLoading];
                                   });
                               }];
}

- (void)startLoading {
  NSError *error;
  AVKeyValueStatus status =
    [self.urlAsset statusOfValueForKey:@"tracks" error:&error];
  if (status == AVKeyValueStatusLoaded) {
    self.assetDuration = (CGFloat) CMTimeGetSeconds(self.urlAsset.duration);
    if (isnan(self.assetDuration)) {
      self.assetDuration = 0;
    }
    self.playerItem = [AVPlayerItem playerItemWithAsset:self.urlAsset];

    [self updateAudioTracks];

    [self.playerItem addObserver:self
                      forKeyPath:@"status"
                         options:NSKeyValueObservingOptionInitial
                         context:&ItemStatusContext];
    [self.playerItem
      addObserver:self
       forKeyPath:@"loadedTimeRanges"
          options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
          context:&ItemStatusContext];
    [[NSNotificationCenter defaultCenter]
                           addObserver:self
                              selector:@selector(playerItemDidReachEnd:)
                                  name:AVPlayerItemDidPlayToEndTimeNotification
                                object:self.playerItem];
    [[NSNotificationCenter defaultCenter]
                           addObserver:self
                              selector:@selector(didFailedToPlayToEnd)
                                  name:AVPlayerItemFailedToPlayToEndTimeNotification
                                object:nil];

    [self.playerItem addOutput:self.videoOutput];

    self.assetPlayer = [AVPlayer playerWithPlayerItem:self.playerItem];

    [self addPeriodicalObserver];
    NSLog(@"Player created");
    if ([self.delegate respondsToSelector:@selector(tracksDidLoad)]) {
      [self.delegate tracksDidLoad];
    }
  } else {
    NSLog(@"The asset's tracks were not loaded:\n%@",
          error.localizedDescription);
  }
}

- (void)updateAudioTracks {
  if (!self.playerItem)
    return;
  for (AVPlayerItemTrack *itemTrack in self.playerItem.tracks) {
    AVAssetTrack *assetTrack = itemTrack.assetTrack;
    if ([assetTrack.mediaType isEqualToString:AVMediaTypeAudio]) {
      [itemTrack setEnabled:self.isAudioEnabled];
    }
  }
}

#pragma mark - Observation

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
  BOOL isOldKey =
    [change[NSKeyValueChangeNewKey] isEqual:change[NSKeyValueChangeOldKey]];

  if (!isOldKey) {
    if (context == &ItemStatusContext) {
      if ([keyPath isEqualToString:@"status"]) {
        [self moviePlayerDidChangeStatus:self.assetPlayer.status];
      } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
        [self moviewPlayerLoadedTimeRangeDidUpdated:self.playerItem
                                                        .loadedTimeRanges];
      }
    }
  }
}

- (void)moviePlayerDidChangeStatus:(AVPlayerStatus)status {
  if (status == AVPlayerStatusFailed) {
    if (self.delegate &&
      [self.delegate respondsToSelector:@selector(didFailLoadVideo)]) {
      [self.delegate didFailLoadVideo];
    }
    NSLog(@"Failed to load video");
  } else if (status == AVPlayerItemStatusReadyToPlay) {
    NSLog(@"Player ready to play");
    self.volume = self.assetPlayer.volume;
    [self.delegate isReadyToPlay];
  } else {
    NSLog(@"Player do not tried to load new media resources for playback yet");
  }
}

- (void)moviewPlayerLoadedTimeRangeDidUpdated:(NSArray *)ranges {
  NSTimeInterval maximum = 0;

  for (NSValue *value in ranges) {
    CMTimeRange range;
    [value getValue:&range];
    NSTimeInterval currenLoadedRangeTime =
      CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration);
    if (currenLoadedRangeTime > maximum) {
      maximum = currenLoadedRangeTime;
    }
  }
  CGFloat progress;
  progress =
    (CGFloat) ((self.assetDuration == 0) ? 0 : maximum / self.assetDuration);


  [self.delegate downloadingProgress:progress];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
  [self.assetPlayer seekToTime:kCMTimeZero];
  [self.assetPlayer play];
}

- (void)didFailedToPlayToEnd {
  NSLog(@"Failed play video to the end");
}

- (void)addPeriodicalObserver {
  CMTime interval = CMTimeMake(1, 100);
  __weak typeof(self) weakSelf = self;
  [self.assetPlayer
    addPeriodicTimeObserverForInterval:interval
                                 queue:dispatch_get_main_queue()
                            usingBlock:^(CMTime time) {
                              [weakSelf playerTimeDidChange:time];
                            }];
}

- (void)playerTimeDidChange:(CMTime)time {
  CGFloat timeNow = (CGFloat) CMTimeGetSeconds(time);

  [self.delegate progressDidUpdate:
                   self.assetDuration == 0 ? 0 :
                     timeNow / self.assetDuration];


  if (self.delegate &&
    [self.delegate
      respondsToSelector:@selector(playerDidChangeProgressTime:
        totalTime:)]) {
    [self.delegate playerDidChangeProgressTime:timeNow
                                     totalTime:self.assetDuration];
  }
}

#pragma mark - Notification

- (void)setupAppNotification __unused {
  [[NSNotificationCenter defaultCenter]
                         addObserver:self
                            selector:@selector(didEnterBackground)
                                name:UIApplicationDidEnterBackgroundNotification
                              object:nil];
  [[NSNotificationCenter defaultCenter]
                         addObserver:self
                            selector:@selector(willEnterForeground)
                                name:UIApplicationWillEnterForegroundNotification
                              object:nil];
}

- (void)didEnterBackground {
  [self.assetPlayer pause];
}

- (void)willEnterForeground {
  [self.assetPlayer pause];
}

#pragma mark - GetImagesFromVideoPlayer

- (BOOL)canProvideFrame {
  return self.assetPlayer.status == AVPlayerItemStatusReadyToPlay;
}

- (CVPixelBufferRef)getCurrentFramePicture {
  CMTime currentTime =
    [self.videoOutput itemTimeForHostTime:CACurrentMediaTime()];
  [self.delegate progressTimeChanged:currentTime];
  if (![self.videoOutput hasNewPixelBufferForItemTime:currentTime]) {
    return 0;
  }
  CVPixelBufferRef buffer =
    [self.videoOutput copyPixelBufferForItemTime:currentTime
                              itemTimeForDisplay:NULL];

  return buffer;
}

- (void)outputMediaDataWillChange:(AVPlayerItemOutput *)sender {
  if (![self.videoOutput hasNewPixelBufferForItemTime:CMTimeMake(1, 10)]) {
    [self configVideoOutput];
    NSLog(@"Rebuild video output...");
  }
}

- (void)configVideoOutput {
  if (self.playerItem && self.videoOutput)
    [self.playerItem removeOutput:self.videoOutput];

  NSDictionary *videoOutputOptions = @{
    [NSValue valueWithPointer:kCVPixelBufferPixelFormatTypeKey] :
    @(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)
  };
  self.videoOutput = [[AVPlayerItemVideoOutput alloc]
                                               initWithPixelBufferAttributes:videoOutputOptions];
  [self.videoOutput setDelegate:self queue:dispatch_get_main_queue()];
  [self.videoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:1];
  [self.playerItem addOutput:self.videoOutput];
}

#pragma mark - CleanUp

- (void)removeObserversFromPlayer {
  @try {
    [self.playerItem removeObserver:self forKeyPath:@"status"];
    [self.playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self.assetPlayer];
  } @catch (NSException *ex) {
    NSLog(@"Cant remove observer in Player - %@", ex.description);
  }
}

- (void)cleanUp {
  [self removeObserversFromPlayer];

  self.assetPlayer.rate = 0;
  self.assetPlayer = nil;
  self.playerItem = nil;
  self.urlAsset = nil;
}

- (void)dealloc {
  [self cleanUp];
}

- (void)enableAudio:(BOOL)enabled {
  self.isAudioEnabled = enabled;
  [self updateAudioTracks];
}

@end
