/**
 360° Player
 Copyright (c) 2016 Giroptic

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import <AVFoundation/AVFoundation.h>

@protocol SPHVideoPlayerDelegate <NSObject>

@optional
- (void)progressDidUpdate:(CGFloat)progress;
- (void)downloadingProgress:(CGFloat)progress;
- (void)progressTimeChanged:(CMTime)time;
- (void)isReadyToPlay;
- (void)playerDidChangeProgressTime:(CGFloat)time
                          totalTime:(CGFloat)totalDuration;
- (void)didFailLoadVideo;
- (void)tracksDidLoad;
@end

@interface SPHVideoPlayer : NSObject

@property(weak, nonatomic) id<SPHVideoPlayerDelegate> delegate;
@property(assign, nonatomic) CGFloat volume;
@property(assign, nonatomic, readonly) CGFloat assetDuration;

- (instancetype)initVideoPlayerWithURL:(NSURL *)urlAsset;
- (void)prepareToPlay;

- (void)play;
- (void)pause;
- (void)stop;
- (void)seekPositionAtProgress:(CGFloat)progressValue
             withPlayingStatus:(BOOL)isPlaying;
- (void)setPlayerVolume:(CGFloat)volume __unused;
- (void)setPlayerRate:(CGFloat)rate __unused;

- (BOOL)canProvideFrame;
- (CVImageBufferRef)getCurrentFramePicture;

- (void)removeObserversFromPlayer;

- (void)enableAudio:(BOOL)enabled;
@end
