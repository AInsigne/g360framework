/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#ifndef Giroptic360PlayerPanoramaView_h
#define Giroptic360PlayerPanoramaView_h

unsigned int PanoramaViewNumVerts = 6;

float PanoramaViewVerts [] = {
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  -0.5, 0.28169014084507, 0,
  0.5, 0.28169014084507, 0,
  0.5, -0.28169014084507, 0,
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  -0.5, 0.28169014084507, 0,
  -0.5, -0.28169014084507, 0,
  0.5, -0.28169014084507, 0,
};

float PanoramaViewNormals [] = {
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  0, 0, -1,
  0, 0, -1,
  0, 0, -1,
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  0, 0, -1,
  0, 0, -1,
  0, 0, -1,
};

float PanoramaViewTexCoords [] = {
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  0.000100, 0.9999,
  0.999900, 0.9999,
  0.999900, 9.9999999999989e-05,
  // f 1/1/1 2/2/1 4/3/1 3/4/1
  0.000100, 0.9999,
  0.000100, 9.9999999999989e-05,
  0.999900, 9.9999999999989e-05,
};
#endif