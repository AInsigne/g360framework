/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHBaseViewController.h"

@interface SPHVideoViewController : SPHBaseViewController <SPHVideoPlayerDelegate>

@property (strong, nonatomic) SPHVideoPlayer *videoPlayer;
@property (assign, nonatomic) BOOL isPlaying;
@property (assign, nonatomic) CGFloat playedProgress;
@property (assign, nonatomic) CGFloat downloadedProgress;

- (void)enableAudio:(BOOL)enabled;

- (void)playVideo;
- (void)pauseVideo;
- (void)stopVideo;
- (void)seekPositionAtProgress:(CGFloat)progressValue withPlayingStatus:(BOOL)status;

@end
