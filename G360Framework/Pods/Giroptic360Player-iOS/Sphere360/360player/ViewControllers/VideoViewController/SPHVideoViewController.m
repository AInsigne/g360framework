/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHVideoViewController.h"

@interface SPHVideoViewController ()

@property(nonatomic) BOOL isAudioEnabled;
@end

@implementation SPHVideoViewController

#pragma mark - Init

- (instancetype)init {
    self = [super initWithMediaType:MediaTypeVideoYUV];
    if (self) {
        self.isAudioEnabled = YES;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder MediaType:MediaTypeVideoYUV];
    if (self) {
        self.isAudioEnabled = YES;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil
                           bundle:nibBundleOrNil
                        MediaType:MediaTypeVideoYUV];
    if (self) {
        self.isAudioEnabled = YES;
    }
    return self;
}

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupVideoPlayer];
}


-(void)enableAudio:(BOOL)enabled{
    self.isAudioEnabled = enabled;
    [self.videoPlayer enableAudio:enabled];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //tbd
    [self clearPlayer];
}

#pragma mark - Draw & update methods

- (void)update
{
    if (self.videoPlayer) {
        [self setNewTextureFromVideoPlayer];
    }
}

#pragma mark - Video

- (void)setupVideoPlayer
{
    NSURL *urlToFile;
    urlToFile = [NSURL URLWithString:self.sourceVideoURL];
    self.videoPlayer = [[SPHVideoPlayer alloc]
                                        initVideoPlayerWithURL:urlToFile];
    [self.videoPlayer enableAudio:self.isAudioEnabled];
    [self.videoPlayer prepareToPlay];
    self.videoPlayer.delegate = self;
}

- (void)setNewTextureFromVideoPlayer
{
    if (self.videoPlayer) {
        if ([self.videoPlayer canProvideFrame]) {
            CVPixelBufferRef pixelBuffer = [self.videoPlayer getCurrentFramePicture];
            if(pixelBuffer)
            {
                [self displayYUVPixelBuffer:pixelBuffer];
                CVPixelBufferRelease(pixelBuffer);
            }
        }
    }
}

#pragma mark - SPHVideoPlayerDelegate

- (void)isReadyToPlay
{
    
}

- (void)progressDidUpdate:(CGFloat)progress
{
    self.playedProgress = progress;
}

- (void)progressTimeChanged:(CMTime)time
{
    
}

- (void)downloadingProgress:(CGFloat)progress
{
    self.downloadedProgress = progress;
}

- (void)playerDidChangeProgressTime:(CGFloat)time totalTime:(CGFloat)totalDuration
{
    
}

#pragma mark - Player

- (void)playVideo
{
    if (![self isPlaying]) {
        [self.videoPlayer play];
        self.isPlaying = YES;
    }
}

- (void)pauseVideo
{
    if ([self isPlaying]) {
        [self.videoPlayer pause];
        self.isPlaying = NO;
    }
}

- (void)stopVideo
{
    if ([self isPlaying]) {
        [self.videoPlayer stop];
        self.isPlaying = NO;
    }
}

- (void)seekPositionAtProgress:(CGFloat)progressValue withPlayingStatus:(BOOL)status
{
    [self.videoPlayer seekPositionAtProgress:progressValue withPlayingStatus:status];
}

#pragma mark - Cleanup

- (void)clearPlayer
{
    [self.videoPlayer stop];
    self.videoPlayer.delegate = nil;
    self.videoPlayer = nil;
}

@end
