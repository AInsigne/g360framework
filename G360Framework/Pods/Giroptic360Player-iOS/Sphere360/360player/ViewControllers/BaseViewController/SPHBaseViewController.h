/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHVideoPlayer.h"

typedef NS_ENUM(NSInteger, MediaType) {
    MediaTypePhoto,
    MediaTypeVideoYUV,
    MediaTypeVideoRGB
};

typedef NS_ENUM(NSInteger, SPHViewModel) {
    OtherModel = -1,
    SphericalModel,
    PlanarModel,
    LittlePlanetModel,
    CubicModel,
    FlatModel
};

@interface SPHBaseViewController : UIViewController

@property (strong, nonatomic) NSString *sourceVideoURL;
@property (strong, nonatomic) UIImage *sourceImage;

@property (assign, nonatomic) MediaType mediaType;

// Spherical by default
@property (assign, nonatomic, readonly) SPHViewModel viewModel;

// Default is YES. If you want to disable rotaton by pan when gyroscope active, set it to NO
@property (assign, nonatomic) BOOL enablePanWhenGyroscopeActive;
@property(nonatomic) BOOL interactionsEnabled;

- (instancetype)initWithMediaType:(MediaType)mediaType;
- (instancetype)initWithCoder:(NSCoder *)aDecoder MediaType:(MediaType)mediaType;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil MediaType:(MediaType)mediaType;

- (void)setupTextureWithImage:(CGImageRef)image;
- (void)displayYUVPixelBuffer:(CVPixelBufferRef)pixelBuffer;

- (void)displayRGBPixelBuffer:(CVPixelBufferRef)pixelBuffer;

- (void)setGyroscopeActive:(BOOL)active;
- (BOOL)isGyroscopeActive;
- (BOOL)isGyroscopeEnabled;

- (void)switchToModel:(SPHViewModel)model;
- (void)applyImageTexture;
- (void)drawArraysGL;
- (void)update;
- (void)startGLUpdates;

- (void)stopGLUpdates;
- (void)displaceX:(CGFloat)x y:(CGFloat)y;

@end
