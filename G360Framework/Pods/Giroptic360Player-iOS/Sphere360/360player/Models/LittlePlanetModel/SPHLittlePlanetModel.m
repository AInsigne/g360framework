/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHLittlePlanetModel.h"

static CGFloat const SPHLittlePlanetAngle = 110.0f;
static CGFloat const SPHLittlePlanetNear = 0.01f;

@implementation SPHLittlePlanetModel

- (void)setupDefaults
{
    self.currentProjectionMatrix = GLKMatrix4Identity;
    self.cameraProjectionMatrix = GLKMatrix4MakeRotation(M_PI_2, 1, 0, 0);
    self.minZoomValue = 0.77f;
    self.maxZoomValue = 1.5f;
    self.defaultZoomValue = 0.77f;
    self.zoomValue = self.defaultZoomValue;
}

- (void)moveToPointX:(CGFloat)pointX andPointY:(CGFloat)pointY
{
    if (self.gyroscopeActive) {
        return;
    }
    pointX *= 0.005;
    
    GLKMatrix4 rotatedMatrix = GLKMatrix4MakeRotation(-pointX / self.zoomValue, 0, 1, 0);
    self.currentProjectionMatrix = GLKMatrix4Multiply(self.currentProjectionMatrix, rotatedMatrix);
}

- (void)update
{
    [super update];
    [self updateMovement];
    CGFloat angle = [super normalizedAngle:SPHLittlePlanetAngle];
    CGFloat near = [super normalizedNear:SPHLittlePlanetNear];

    CGRect viewFrame = self.baseController.view.frame;
    CGFloat aspect = fabs(viewFrame.size.width / viewFrame.size.height);
    
    CGFloat cameraDistanse = - (self.zoomValue - self.maxZoomValue);
    if (self.baseController.interfaceOrientation == UIInterfaceOrientationPortrait) {
        angle += 7; // fix aspect difference in portrait & landscape
    }
    CGFloat FOVY = GLKMathDegreesToRadians(angle) / self.zoomValue;

    GLKMatrix4 cameraTranslation = GLKMatrix4MakeTranslation(0, 0, -cameraDistanse / 2.0);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(FOVY, aspect, near, 2.4);
    projectionMatrix = GLKMatrix4Multiply(projectionMatrix, cameraTranslation);
    GLKMatrix4 modelViewMatrix = GLKMatrix4Identity;

    projectionMatrix = GLKMatrix4Multiply(projectionMatrix, self.cameraProjectionMatrix);
    modelViewMatrix = self.currentProjectionMatrix;
    self.modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
}

#pragma mark - Private

- (void)updateMovement
{
    if (CGPointEqualToPoint(self.velocity, CGPointZero)) {
        return;
    }
    self.velocity = CGPointMake(0.9 * self.velocity.x, 0.9 * self.velocity.y);
    CGPoint nextPoint = CGPointMake(SPHVelocityCoef * self.velocity.x, SPHVelocityCoef * self.velocity.y);
    if (fabs(nextPoint.x) < 0.1 && fabs(nextPoint.y) < 0.1) {
        self.velocity = CGPointZero;
    }
    [self moveToPointX:nextPoint.x andPointY:nextPoint.y];
}

@end
