/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHPlanarModel.h"

static CGFloat const SPHPlanarAngle = 70.0f;
static CGFloat const SPHPlanarNear = 0.1f;

@interface SPHPlanarModel ()

@end

@implementation SPHPlanarModel {
    CGFloat xMove;
}

#pragma mark - Custom Accessors

- (void)setGyroscopeActive:(BOOL)gyroscopeActive
{
    [super setGyroscopeActive:NO];
}

- (CMMotionManager *)motionManager
{
    return nil;
}

#pragma mark - Override

- (void)setupDefaults
{
    self.currentProjectionMatrix = GLKMatrix4Identity;
    self.cameraProjectionMatrix = GLKMatrix4Identity;
    self.minZoomValue = 1.2f;
    self.maxZoomValue = 2.7f;
    self.defaultZoomValue = 1.1f;
    self.zoomValue = self.defaultZoomValue;
}

- (void)update
{
    [super update];
    [self updateMovement];
    
    CGFloat angle = [super normalizedAngle:SPHPlanarAngle];
    CGFloat near = [super normalizedNear:SPHPlanarNear];
    
    CGRect viewFrame = self.baseController.view.frame;
    
    CGFloat FOVY = GLKMathDegreesToRadians(angle) / self.defaultZoomValue;
    CGFloat aspect = fabs(viewFrame.size.width / viewFrame.size.height);

    CGFloat cameraDistanse = - (self.defaultZoomValue - 1.7f);
    
    GLKMatrix4 cameraTranslation = GLKMatrix4MakeTranslation(0, 0, -cameraDistanse / 1.4);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(FOVY, aspect, near, 2.4);
    projectionMatrix = GLKMatrix4Multiply(projectionMatrix, cameraTranslation);
    projectionMatrix = GLKMatrix4Multiply(projectionMatrix, self.cameraProjectionMatrix);
    self.modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, self.currentProjectionMatrix);
    self.modelViewProjectionMatrixInfinite = self.modelViewProjectionMatrix;
    
    if (self.modelViewProjectionMatrix.m30 >= self.modelViewProjectionMatrix.m00 || self.modelViewProjectionMatrix.m30 <= self.modelViewProjectionMatrix.m00) {
        GLKMatrix4 projectionMatrix = self.modelViewProjectionMatrix;
        projectionMatrix.m30 = fmodf(self.modelViewProjectionMatrix.m30 , self.modelViewProjectionMatrix.m00);
        self.modelViewProjectionMatrix = projectionMatrix;
    }
    
    GLKMatrix4 modelProjectionMatrix2 = self.modelViewProjectionMatrix;
    if (self.modelViewProjectionMatrix.m30 > 0) {
        modelProjectionMatrix2.m30 = self.modelViewProjectionMatrix.m30 - self.modelViewProjectionMatrix.m00;
    } else {
        modelProjectionMatrix2.m30 = self.modelViewProjectionMatrix.m30 + self.modelViewProjectionMatrix.m00;
    }
    self.modelViewProjectionMatrixInfinite = modelProjectionMatrix2;
}

- (void)moveToPointX:(CGFloat)pointX andPointY:(CGFloat)pointY
{
    if (self.gyroscopeActive || [self isMoveDisabled]) {
        return;
    }
    pointX *= 0.0005;
    xMove += pointX;
    GLKMatrix4 newMatrix = self.currentProjectionMatrix;
    newMatrix.m30 = xMove;
    self.currentProjectionMatrix = newMatrix;
}

- (void)updateMovement
{
    if (CGPointEqualToPoint(self.velocity, CGPointZero) || [self isMoveDisabled]) {
        return;
    }
    self.velocity = CGPointMake(0.9 * self.velocity.x, 0.9 * self.velocity.y);
    CGPoint nextPoint = CGPointMake(SPHVelocityCoef * self.velocity.x, SPHVelocityCoef * self.velocity.y);
    if (fabs(nextPoint.x) < 0.1 && fabs(nextPoint.y) < 0.1) {
        self.velocity = CGPointZero;
    }
    [self moveToPointX:nextPoint.x andPointY:nextPoint.y];
}

#pragma mark - Private

- (BOOL)isMoveDisabled
{
    BOOL enabled = NO;
    CGRect viewFrame = self.baseController.view.frame;
    if (viewFrame.size.width > viewFrame.size.height) {
        enabled = YES;
    }
    return enabled;
}

@end