/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHBaseModel.h"

@implementation SPHBaseModel

#pragma mark - Init

- (instancetype)initWithViewController:(SPHBaseViewController *)viewController
                                GLView:(GLKView *)glView;
{
    self = [self init];
    if (self) {
        self.baseController = viewController;
        self.glView = glView;
        [self setupDefaults];
    }
    return self;
}

#pragma mark - Custom Accessors

- (void)setGyroscopeActive:(BOOL)gyroscopeActive
{
    _gyroscopeActive = gyroscopeActive;
}

- (CMMotionManager *)motionManager
{
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    return _motionManager;
}

#pragma mark - Zoom

- (void)updateZoomValue
{
    if (self.zoomValue > self.maxZoomValue) {
        self.isUpdatingZoom = YES;
        self.zoomValue *= 0.99;
    } else if (self.zoomValue <  self.minZoomValue) {
        self.isUpdatingZoom = YES;
        self.zoomValue *= 1.01;
    } else {
        self.isUpdatingZoom = NO;
    }
}

#pragma mark - Gyroscope

- (BOOL)isGyroscopeEnabled
{
    return NO;
}

#pragma mark - Override

- (void)setupDefaults
{
    // for subclassing
}

- (void)moveToPointX:(CGFloat)pointX andPointY:(CGFloat)pointY
{
    // for subclassing
}

- (void)update
{
    if (!self.isZooming) {
        [self updateZoomValue];
    }
}

#pragma mark - Projection

- (CGFloat)normalizedAngle:(CGFloat)normalAngle
{
    if (!self.angle) {
        self.angle = normalAngle;
        return normalAngle;
    }
    switch (self.baseController.viewModel) {
        case SphericalModel: {
            if (self.angle > normalAngle) {
                self.angle--;
            }
            break;
        }
        case LittlePlanetModel: {
            if (self.angle < normalAngle) {
                self.angle++;
            }
            break;
        }
        default: {
            self.angle = normalAngle;
            break;
        }
    }
    return self.angle;
}

- (CGFloat)normalizedNear:(CGFloat)normalNear
{
    if (!self.near) {
        self.near = normalNear;
        return normalNear;
    }
    switch (self.baseController.viewModel) {
        case SphericalModel: {
            if (self.near < normalNear) {
                self.near+=0.005;
            }
            break;
        }
        case LittlePlanetModel: {
            if (self.near > normalNear) {
                self.near-=0.005;
            }
            break;
        }
        default: {
            self.near = normalNear;
            break;
        }
    }
    return self.near;
}

@end
