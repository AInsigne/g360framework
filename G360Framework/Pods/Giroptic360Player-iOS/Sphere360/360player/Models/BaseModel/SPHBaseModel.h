/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHBaseViewController.h"
#import <GLKit/GLKit.h>
#import <CoreMotion/CoreMotion.h>

static CGFloat SPHVelocityCoef = 0.01f;

@interface SPHBaseModel : NSObject

- (instancetype)initWithViewController:(SPHBaseViewController *)viewController
                                GLView:(GLKView *)glView;

@property (weak, nonatomic) SPHBaseViewController *baseController;
@property (weak, nonatomic) GLKView *glView;

// zoom properties
@property (assign, nonatomic) CGFloat maxZoomValue;
@property (assign, nonatomic) CGFloat minZoomValue;
@property (assign, nonatomic) CGFloat defaultZoomValue;
@property (assign, nonatomic) CGFloat zoomValue;

@property (assign, nonatomic) BOOL isZooming;
@property (assign, nonatomic) BOOL isUpdatingZoom;

// projection matrix
@property (assign, nonatomic) CGFloat angle;
@property (assign, nonatomic) CGFloat near;

@property (assign, nonatomic) GLKMatrix4 modelViewProjectionMatrix;
@property (assign, nonatomic) GLKMatrix4 modelViewProjectionMatrixInfinite;

@property (assign, nonatomic) GLKMatrix4 currentProjectionMatrix;
@property (assign, nonatomic) GLKMatrix4 cameraProjectionMatrix;

// gyroscope
@property (assign, nonatomic) BOOL gyroscopeActive;
@property (strong, nonatomic) CMMotionManager *motionManager;

// velocity
@property (assign, nonatomic) CGPoint velocity;

- (BOOL)isGyroscopeEnabled;

- (void)setupDefaults;
- (void)update;

- (void)moveToPointX:(CGFloat)pointX andPointY:(CGFloat)pointY;

- (CGFloat)normalizedAngle:(CGFloat)normalAngle;
- (CGFloat)normalizedNear:(CGFloat)normalNear;

@end
