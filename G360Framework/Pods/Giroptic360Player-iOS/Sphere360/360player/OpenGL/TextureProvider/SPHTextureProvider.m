/**
 360° Player
 Copyright (c) 2016 Giroptic
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

#import "SPHTextureProvider.h"
#import <CoreImage/CoreImage.h>

@implementation SPHTextureProvider

+ (CGImageRef)imageWithCVPixelBufferUsingUIGraphicsContext:(CVPixelBufferRef)pixelBuffer
{
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    int width = (int)CVPixelBufferGetWidth(pixelBuffer);
    int hight = (int)CVPixelBufferGetHeight(pixelBuffer);
    int rows = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
    int bytesPerPixel = rows/width;
    
    unsigned char *bufferPointer = CVPixelBufferGetBaseAddress(pixelBuffer);
    UIGraphicsBeginImageContext(CGSizeMake(width, hight));
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    unsigned char* data = CGBitmapContextGetData(context);
    if (data) {
        int maxY = hight;
        for(int y = 0; y < maxY; y++) {
            for(int x = 0; x < width; x++) {
                int offset = bytesPerPixel*((width*y)+x);
                data[offset] = bufferPointer[offset];     // R
                data[offset+1] = bufferPointer[offset+1]; // G
                data[offset+2] = bufferPointer[offset+2]; // B
                data[offset+3] = bufferPointer[offset+3]; // A
            }
        }
    }
    CGImageRef cgImage = CGBitmapContextCreateImage(context);
    UIGraphicsEndImageContext();

    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CVBufferRelease(pixelBuffer);
    
    return cgImage;
}

@end
