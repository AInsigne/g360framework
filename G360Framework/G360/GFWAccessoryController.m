//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <GFWAccessoryKit/GFWAccessoryKit.h>
#import "GFWAccessoryController.h"
#import "GFWServiceLocator.h"


@implementation GFWAccessoryController {

}
- (void)start {
    id <GFWAccessoryService> service;
    service = [GFWServiceLocator sharedInstance].accessoryService;

    [service startWithAccessoryConnection:^(id <GFWAccessory> accessory) {

     }
                            disconnection:^(id <GFWAccessory> accessory) {

                            }];
}

- (void)stop {
    id <GFWAccessoryService> service;
    service = [GFWServiceLocator sharedInstance].accessoryService;
    [service stop];
}


@end