//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <CoreMedia/CoreMedia.h>
#import <Foundation/Foundation.h>

@interface GFWPixelBufferQueueElement : NSObject
@property(nonatomic) CMTime pts;
@property(nonatomic) CVPixelBufferRef pixelBuffer;
@property(nonatomic) OSStatus status;
@end