//
// Created by Maxime CHAPELET on 27/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol GFWFakeAccessoryInputStreamBuffer;

@interface GFWFakeAccessoryInputStream : NSInputStream
- (instancetype)initWithStreamBuffer:(id<GFWFakeAccessoryInputStreamBuffer>)streamBuffer;
@end