//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFW360Player.h"

@interface GFW360Player ()
@property(nonatomic, assign) CVPixelBufferRef pixelBuffer;
@end
@implementation GFW360Player {
}

#pragma mark - Init

- (instancetype)init {
  self = [super initWithMediaType:MediaTypeVideoRGB];
  if (self) {
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder MediaType:MediaTypeVideoYUV];
  if (self) {
  }
  return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil
                         bundle:nibBundleOrNil
                      MediaType:MediaTypeVideoYUV];
  if (self) {
  }
  return self;
}

#pragma mark - Draw & update methods

- (void)update {
    
  if (self.pixelBuffer) {
    [self displayYUVPixelBuffer:self.pixelBuffer];
    self.pixelBuffer = NULL;
  }
}

- (void)setPixelBuffer:(CVPixelBufferRef)pixelBuffer {
  if (_pixelBuffer != pixelBuffer) {
    if (_pixelBuffer)
      CVPixelBufferRelease(_pixelBuffer);
    _pixelBuffer = pixelBuffer;
    if (pixelBuffer) {
      CVPixelBufferRetain(pixelBuffer);
    }
  }
}

- (void)processPixelBuffer:(CVPixelBufferRef)pixelBuffer {
  [self setPixelBuffer:pixelBuffer];
}

@end