//
//  GFWDataPool.m
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 09/08/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWDataPool.h"

NSUInteger const kMaxPacketSize = 1 << 16;

@interface GFWDataPool ()
@property(nonatomic, strong) NSMutableArray *available;
@property (nonatomic,strong)dispatch_queue_t isolationQueue;
@end

@implementation GFWDataPool

- (instancetype)init {
  self = [super init];
  if (self) {
    self.available = [NSMutableArray new];
    self.isolationQueue = dispatch_queue_create("datapool.isolationqueue",NULL);
  }
  return self;
}

- (NSMutableData *)retrieveData {
  __block NSMutableData *data = nil;
  dispatch_sync(self.isolationQueue,^{
    if (self.available.count) {
      data = [self.available lastObject];
      [self.available removeLastObject];
    }
  });
  if(!data){
    data = [[NSMutableData alloc] initWithCapacity:kMaxPacketSize];
  }
  data.length = 0;
  return data;
}

- (void)returnData:(NSMutableData *)data {
  if (data) {
    dispatch_sync(self.isolationQueue,^{
      [self.available addObject:data];
    });
  }
}

@end
