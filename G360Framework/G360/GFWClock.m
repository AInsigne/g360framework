//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFWClock.h"

@implementation GFWClock {
  CMTimebaseRef _tmBase;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    _tmBase = NULL;
  }

  return self;
}

- (void)start {
  [self stop];
  CMTimebaseCreateWithMasterClock(NULL, CMClockGetHostTimeClock(), &_tmBase);
  CMTimebaseSetTime(_tmBase, kCMTimeZero);
  CMTimebaseSetRate(_tmBase, 1.0);
}

- (void)stop {
  if (_tmBase) {
    CFRelease(_tmBase);
    _tmBase = NULL;
  }
}
- (void)setTime:(CMTime)cmTime {
  CMTimebaseSetTime(_tmBase, cmTime);
}

- (void)getTime:(CMTime *const)cmTime {
  CMTime tbTime = CMTimebaseGetTime(_tmBase);
  if (cmTime) {
    cmTime->epoch = tbTime.epoch;
    cmTime->flags = tbTime.flags;
    cmTime->timescale = tbTime.timescale;
    cmTime->value = tbTime.value;
  }
}

- (void)dealloc {
  [self stop];
}

@end