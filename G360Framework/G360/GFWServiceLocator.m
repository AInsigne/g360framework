//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <GFWAccessoryKit/GFWAccessoryKit.h>
#import "GFWServiceLocator.h"
#import "GFWFakeAccessoryService.h"
#import "GFWAVSyncService.h"

@interface GFWServiceLocator ()
@property(nonatomic, readwrite, strong) id <GFWAccessoryService> accessoryService;
@property(nonatomic, readwrite, strong) GFWAVSyncService* avsyncService;
@end

@implementation GFWServiceLocator {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.accessoryService = [GFWFakeAccessoryService new];
        self.avsyncService = [GFWAVSyncService new];
        /*
        BOOL fakeExternalAccessory = [[NSUserDefaults standardUserDefaults]
                boolForKey:@"fakeExternalAccessory"];
        if(fakeExternalAccessory){
            self.accessoryService = [GFWFakeAccessoryService new];
        } else {
            self.accessoryService = [GFWAccessoryService new];
        }
        */
    }
    return self;
}


+ (instancetype)sharedInstance {
    static GFWServiceLocator *serviceLocator;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        serviceLocator = [GFWServiceLocator new];
    });
    return serviceLocator;
}


@end
