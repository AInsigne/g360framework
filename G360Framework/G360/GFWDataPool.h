//
//  GFWDataPool.h
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 09/08/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GFWDataPool : NSObject

- (NSMutableData *)retrieveData;
- (void)returnData:(NSMutableData *)data;

@end
