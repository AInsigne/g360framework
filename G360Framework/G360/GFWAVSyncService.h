//
//  GFWAVSyncService.h
//  360Live
//
//  Created by Maxime CHAPELET on 22/09/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface GFWAVSyncService : NSObject
@property (nonatomic, assign) uint64_t videoTS;
@property (nonatomic, assign) uint64_t audioTS;
@end
