//
// Created by Giroptic on 10/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>

extern NSString *const gfwnaluTypesStrings[];

@interface GFWAccessoryNALU : NSObject
@property(nonatomic) uint32_t offset;
@property(nonatomic) uint32_t type;
@property(nonatomic, strong) NSMutableData *data;
@property(nonatomic) uint64_t timestamp;
@end