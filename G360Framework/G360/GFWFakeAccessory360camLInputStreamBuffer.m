//
// Created by Giroptic on 01/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <CocoaLumberjack/DDLogMacros.h>
#import <GFWAccessoryKit/GFWAccessoryConstants.h>
#import "GFWFakeAccessory360camLInputStreamBuffer.h"
#import "GFWClock.h"
#import "GFWLiveProtocolPacket.h"
#import "qbox.h"

#define BE_32(x)                                                               \
  (((uint32_t)(((uint8_t *)(x))[0]) << 24) | (((uint8_t *)(x))[1] << 16) |     \
   (((uint8_t *)(x))[2] << 8) | ((uint8_t *)(x))[3])

#define FOURCC(a, b, c, d)                                                     \
  ((uint32_t)(unsigned char)(d) << 24 | ((uint32_t)(unsigned char)(c) << 16) | \
   ((uint32_t)(unsigned char)(b) << 8) | ((uint32_t)(unsigned char)(a)))

#define TYPE_QBOX FOURCC('q', 'b', 'o', 'x')

typedef struct {
  uint32_t size;
  uint32_t type;
} QboxPacketHeader;

@interface GFWFakeAccessory360camLInputStreamBuffer ()
@property(nonatomic, strong) NSFileHandle *fileHandle;
@property(nonatomic, strong) NSMutableData *mutableBuffer;
@property(nonatomic) unsigned long long int fileSize;
@property(nonatomic, strong) NSMutableArray *packets;
@property(nonatomic, assign) NSUInteger currentPacket;
@end
@implementation GFWFakeAccessory360camLInputStreamBuffer {
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.fileHandle = [NSFileHandle fileHandleForReadingAtPath:[self filePath]];
    NSDictionary *dictionary;
    dictionary =
        [[NSFileManager defaultManager] attributesOfItemAtPath:[self filePath]
                                                         error:nil];
    self.fileSize = [dictionary[NSFileSize] unsignedLongLongValue];
    self.mutableBuffer = [NSMutableData new];
    [self findPackets:self.fileHandle size:self.fileSize];
  }

  return self;
}

- (void)findPackets:(NSFileHandle *)fileHandle size:(NSUInteger)size {
  GFWLiveProtocolPacket *packet = nil;
  NSData *readData = nil;
  QboxPacketHeader const *header = NULL;
  uint32_t hdrlen = sizeof(QboxPacketHeader);
  self.packets = [NSMutableArray new];
  uint64_t prevTimeStamp = 0;
  while (fileHandle.offsetInFile < size) {
    readData = [fileHandle readDataOfLength:hdrlen];
    uint32_t box_size = 1;
    header = readData.bytes;
    if (header->type == TYPE_QBOX) {
      box_size = BE_32(&(header->size));

      [fileHandle seekToFileOffset:(fileHandle.offsetInFile - hdrlen)];
      readData = [fileHandle readDataOfLength:box_size];
      packet = [GFWLiveProtocolPacket new];
      packet.size = box_size;
      packet.offset = fileHandle.offsetInFile - box_size;

      int channelID = 0;
      video_format_t fmt = VID_FORMAT_H264_RAW;
      uint8_t *data_buf = NULL;
      uint64_t timeStamp = 0;
      metadata_t metadata;
      uint32_t analytics = 0;
      uint32_t data_size = 0;
      qbox_parse_header(readData.bytes, &channelID, &fmt, &data_buf, &data_size,
                        &timeStamp, &analytics, &metadata);
      packet.timestamp = timeStamp;
      DDLogDebug(@"Timestamp :%llu Delay: %llu", timeStamp, timeStamp-prevTimeStamp);
      [self.packets addObject:packet];
      prevTimeStamp = timeStamp;
    }
  }
}

- (void)prefill {
}

- (void)update {
  if (self.mutableBuffer.length == 0) {
    GFWLiveProtocolPacket *packet = self.packets[self.currentPacket];
    [self.fileHandle seekToFileOffset:packet.offset];
    NSData *data = [self.fileHandle readDataOfLength:packet.size];
    [self.mutableBuffer appendData:data];
    self.currentPacket++;
    self.currentPacket = self.currentPacket % self.packets.count;
  }
}

- (NSUInteger)length {
  return self.mutableBuffer.length;
}

- (void)getBytes:(uint8_t *)buffer length:(NSUInteger)length {
  [self.mutableBuffer getBytes:buffer length:length];
  [self.mutableBuffer replaceBytesInRange:NSMakeRange(0, length)
                                withBytes:NULL
                                   length:0];
}

- (NSString *)filePath {
//  return [[NSBundle mainBundle]
//      pathForResource:@"Samples/5192AE3A-BAF3-4F76-A31A-CE510546E49E-2016-09-07--16-29-25"
//               ofType:@"qbox"];
    return [[NSBundle mainBundle]
            pathForResource:@"Samples/C9A5CF6C-E70C-4A23-8F4B-13305FC0618F-2016-09-21--15-17-35"
            ofType:@"qbox"];
}
@end