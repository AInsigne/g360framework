//
// Created by Maxime CHAPELET on 23/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>

@class GFWFakeAccessoryInputStream;

@interface GFWFakeAccessorySession : NSObject <GFWAccessorySession>
@end