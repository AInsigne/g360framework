//
// Created by Giroptic on 21/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>

@class GFWAccessoryNALU;

@protocol GFWAccessoryNALUsProcessor <NSObject>

@required
- (void)start;
- (void)processNALUs:(NSArray<GFWAccessoryNALU *> *)nalus;
- (void)stop;
@optional
- (void)processData:(uint8_t *)buf
               size:(uint32_t)size
          timestamp:(uint64_t)timestamp;
-(void)processData:(NSData*)data timestamp:(uint64_t)timestamp;
@end