//
//  ControllerProtocols.h
//  POC2
//
//  Created by Naz Mariano on 09/09/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//
#import <Foundation/Foundation.h>
#ifndef ControllerProtocols_h
#define ControllerProtocols_h

#define weakify(var) __weak typeof(var) AHKWeak_##var = var;

#define strongify(var) \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__strong typeof(var) var = AHKWeak_##var; \
_Pragma("clang diagnostic pop")

@protocol ViewLifeCycle <NSObject>

-(void)viewDidLoad;

@end

#endif /* ControllerProtocols_h */
