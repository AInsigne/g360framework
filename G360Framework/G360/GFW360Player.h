//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Giroptic360Player-iOS-umbrella.h"
#import "GFW360camLH264RawNALUsParser.h"

@protocol GFWAccessoryPixelBufferProcessor <NSObject>

- (void)processPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end




@interface GFW360Player: SPHBaseViewController<GFWAccessoryPixelBufferProcessor>

@end