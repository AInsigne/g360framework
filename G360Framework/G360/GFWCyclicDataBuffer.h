//
// Created by Maxime CHAPELET on 27/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GFWCyclicDataBuffer : NSObject
@property(nonatomic, readonly) NSUInteger emptySpace;
@property(nonatomic, readonly) NSUInteger size;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithSize:(NSUInteger)size;

+ (instancetype)new NS_UNAVAILABLE;

+ (instancetype)createWithSize:(NSUInteger)size;

- (BOOL)canWriteDataWithLength:(NSUInteger)length;

- (BOOL)canReadData;

- (NSUInteger)spaceFilled;

- (BOOL)writeData:(uint8_t const *)data length:(NSUInteger)length;

- (BOOL)readByte:(uint8_t *)byte;
- (NSInteger)readData:(uint8_t *)buffer length:(size_t)length;
@end