//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kExternalAccessoryProtocol;
extern NSString *const kAccessoryDisconnectedNotification;

extern uint8_t const kEndOfStreamPacketSize;
extern uint8_t const kEndOfStreamPacket[8];