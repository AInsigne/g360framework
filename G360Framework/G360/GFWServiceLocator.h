//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GFWAccessoryService;
@class GFWAVSyncService;

@interface GFWServiceLocator : NSObject

@property(nonatomic, readonly, strong) id<GFWAccessoryService> accessoryService;
@property(nonatomic, readonly, strong) GFWAVSyncService *avsyncService;
+ (instancetype)sharedInstance;

@end