//
//  GFW360camLElementaryStream.m
//  GFWAccessoryKit
//
//  Created by Naz Mariano on 06/09/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFW360camLElementaryStream.h"
#import "GFWAccessoryNALU.h"
#import "GFWAVSyncService.h"
#import "GFWServiceLocator.h"
#import <CocoaLumberjack/DDLogMacros.h>
#import <GFWAccessoryKit/GFWAccessoryConstants.h>


@interface GFW360camLElementaryStream()
@property(nonatomic, strong) GFWAccessoryNALU *sps;
@property(nonatomic, strong) GFWAccessoryNALU *pps;
@property (weak, nonatomic) id<EncoderReceiver> receiver;
@property (assign, nonatomic) BOOL streamStarted;
@property (strong, nonatomic) GFW360camLH264ParametersSets *parameterSets;
@property (assign) BOOL parameterSetsSent;
@end
@implementation GFW360camLElementaryStream

-(instancetype)initWithSocket:(id<EncoderReceiver>)aReceiver
{
    self = [super init];
    if (self) {
        self.receiver = aReceiver;
    }
    return self;
}

- (void)start {
    
}

- (void)processNALUs:(NSArray<GFWAccessoryNALU *> *)nalus {
    
    [self convert:nalus];
}

- (void)convert:(NSArray<GFWAccessoryNALU *> *)nalus {
    if (nalus.count == 0) {
        return;
    }

    // Proceed nalus
    NSUInteger i = 0;
    for (i = 0; i < nalus.count; i++) {
        GFWAccessoryNALU *nalu = nalus[i];
        
        if (nalu.type != 0x9) {
//            SInt64 ptsMicroAdjusted = nalu.timestamp;

            Float64 ptsSecs = nalu.timestamp / 90000.0;
            UInt64 ptsMicroSecs = (UInt64) (ptsSecs * 1000000.0);

            GFWAVSyncService *service =
              [GFWServiceLocator sharedInstance].avsyncService;
            if(service.videoTS == 0){
              service.videoTS = ptsMicroSecs;
            }
//            NSLog(@"Video TS %llu",ptsMicroSecs);
            ptsMicroSecs = CFSwapInt64HostToBig(ptsMicroSecs);
            
            if (self.parameterSets) {
                NSMutableData *ppsStream = [NSMutableData data];
                [ppsStream appendBytes:&ptsMicroSecs length:8];
                [ppsStream appendData:self.parameterSets.pps];
                
                if ([self.receiver isConnected]) {
                    [self.receiver sendVideoData:ppsStream];
                }
                
                NSMutableData *spsStream = [NSMutableData data];
                [spsStream appendBytes:&ptsMicroSecs length:8];
                [spsStream appendData:self.parameterSets.sps];
                
                if ([self.receiver isConnected]) {
                    [self.receiver sendVideoData:ppsStream];
                }
            }
            
            NSMutableData *videoStream = [NSMutableData data];
            [videoStream appendBytes:&ptsMicroSecs length:8];
            NSData *naluCopy = [nalu.data copy];
            [videoStream appendData:naluCopy];
            if ([self.receiver isConnected]) {
                [self.receiver sendVideoData:videoStream];
            }
        }
    }
    
    
    if (!self.streamStarted) {
        self.streamStarted = YES;
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(cameraStreamBecameActive)]) {
                [self.delegate cameraStreamBecameActive];
            }
        }
    }
}

- (void)h264ParameterSetsDidChange:(GFW360camLH264ParametersSets *)h264ParameterSets {
    self.parameterSets = h264ParameterSets;
    self.parameterSetsSent = NO;
}

-(void)processData:(NSData*)data timestamp:(uint64_t)timestamp{


//    static const uint8_t startCode[] = {0x00, 0x00, 0x00, 0x01};
//    SInt64 ptsMicroAdjusted = CFSwapInt64HostToBig(timestamp);
//    SInt64 ptsMicroAdjusted = timestamp;
//    for (uint32_t i = 0; i < size; i++) {
//        uint32_t value = *((uint32_t *)(buf + i));
//        if (value == 0x01000000) {
//            uint8_t type = (*(buf + i + 4)) & ((uint8_t)0x1F);

//            NSMutableData *videoStream = [NSMutableData data];
//            [videoStream appendBytes:&ptsMicroAdjusted length:8];
//    
//            [videoStream appendData:data];
//            
//            if ([self.receiver isConnected]) {
//                [self.receiver sendVideoData:videoStream];
//            }
//        }
//    }
}

- (void)stop {
    
}

-(void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer formatDescriptionChanged:(BOOL)formatDescriptionChanged isIDR:(BOOL)isIDR{}

//#pragma mark GFWAccessorySampleBufferProcessor
-(void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer formatDescriptionChanged:(BOOL)formatDescriptionChanged {

}
@end
