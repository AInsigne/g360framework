//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GFWAccessoryController : NSObject

- (void)start;

- (void)stop;

@end