//
// Created by Maxime CHAPELET on 27/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWFakeAccessoryInputStream.h"
#import "GFWFakeAccessoryInputStreamBuffer.h"

@interface GFWFakeAccessoryInputStream () <NSStreamDelegate>
@property(nonatomic, weak) id<NSStreamDelegate> fakeDelegate;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic) BOOL isOpened;
@property(nonatomic, strong) NSTimer *fillBufferTimer;
@property(nonatomic, strong) id<GFWFakeAccessoryInputStreamBuffer> streamBuffer;
@property(nonatomic, strong) dispatch_queue_t bufferQueue;
@end

@implementation GFWFakeAccessoryInputStream {
  int readIndex;
}

- (instancetype)initWithStreamBuffer:
    (id<GFWFakeAccessoryInputStreamBuffer>)streamBuffer {
  self = [super init];
  if (self) {
    self.fakeDelegate = self;
    self.bufferQueue =
        dispatch_queue_create("fakeinputstream.bufferqueue", NULL);
    self.streamBuffer = streamBuffer;
    [self.streamBuffer prefill];
    readIndex = 0;
  }

  return self;
}

- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)len {
  __block NSUInteger readSize;
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.bufferQueue, ^{
    readSize = MIN(len, weakSelf.streamBuffer.length);
    if (buffer && readSize > 0) {
      [weakSelf.streamBuffer getBytes:buffer length:readSize];
    }
  });
  return readSize;
}

- (BOOL)getBuffer:(uint8_t *__nullable *__nonnull)buffer
           length:(NSUInteger *)len {
  return NO;
}

- (BOOL)hasBytesAvailable {
  __block NSUInteger length;
  __weak typeof(self) weakSelf = self;
  dispatch_sync(self.bufferQueue, ^{
    length = weakSelf.streamBuffer.length;
  });
  return length > 0;
}

- (void)setDelegate:(id<NSStreamDelegate>)delegate {
  self.fakeDelegate = delegate;
}

- (id<NSStreamDelegate>)delegate {
  return self.fakeDelegate;
}

- (void)scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
  self.timer = [NSTimer timerWithTimeInterval:0.016f
                                       target:self
                                     selector:@selector(update:)
                                     userInfo:nil
                                      repeats:YES];
  [aRunLoop addTimer:self.timer forMode:mode];
}

- (void)removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
  [self.timer invalidate];
}

- (void)update:(id)update {
  if ([self.fakeDelegate respondsToSelector:@selector(stream:handleEvent:)]) {
    if (self.hasBytesAvailable) {
      [self.fakeDelegate stream:self
                    handleEvent:NSStreamEventHasBytesAvailable];
    }
  }
}

- (void)open {
  self.isOpened = YES;
  self.fillBufferTimer =
      [NSTimer scheduledTimerWithTimeInterval:0.033f
                                       target:self
                                     selector:@selector(fillBuffer)
                                     userInfo:nil
                                      repeats:YES];
}

- (void)fillBuffer {
  __weak typeof(self) weakSelf = self;
  dispatch_async(self.bufferQueue, ^{
    [weakSelf.streamBuffer update];
  });
}

- (void)close {
  self.isOpened = NO;
  [self.timer invalidate];
  [self.fillBufferTimer invalidate];
}

- (NSStreamStatus)streamStatus {
  if (self.isOpened) {
    return NSStreamStatusOpen;
  } else {
    return NSStreamStatusClosed;
  }
}

@end