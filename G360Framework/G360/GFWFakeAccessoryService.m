//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWFakeAccessoryService.h"
#import "GFWFakeAccessory.h"
#import "GFWFakeAccessorySession.h"

@interface GFWFakeAccessoryService ()
@property(nonatomic, copy) void (^connectionBlock)(id <GFWAccessory>);
@property(nonatomic, copy) void (^disconnectionBlock)(id <GFWAccessory>);
@property(nonatomic, assign) BOOL isAccessoryConnected;
@end

@implementation GFWFakeAccessoryService {

}
- (void)stop {
    self.connectionBlock = nil;
    self.disconnectionBlock = nil;
    DDLogInfo(@"Stopping Accessory Service.");
    [DDLog removeLogger:[GFWAccessoryLoggingController fileLogger]];
}

- (void)startWithAccessoryConnection:(void (^)(id <GFWAccessory> accessory))connectionBlock
                       disconnection:(void (^)(id <GFWAccessory> accessory))disconnectionBlock {
    [DDLog addLogger:[GFWAccessoryLoggingController fileLogger]];
    DDLogInfo(@"Starting Accessory Service.");
    self.connectionBlock = connectionBlock;
    self.disconnectionBlock = disconnectionBlock;
    [self observeConnectionEvents];
}

- (void)observeConnectionEvents {
    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(applicationDidBecomeActive:)
                   name:UIApplicationDidBecomeActiveNotification
                 object:nil];
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    self.isAccessoryConnected = !self.isAccessoryConnected;
    if(self.isAccessoryConnected){
        [self accessoryDidConnect];
    } else{
        [self accessoryDidDisconnect];
    }
}

- (void)accessoryDidDisconnect {
    GFWFakeAccessory * accessory = [GFWFakeAccessory new];
    DDLogInfo(@"Accessory did connect : %@", accessory);
    if(self.disconnectionBlock){
        self.disconnectionBlock(accessory);
    }
}

- (void)accessoryDidConnect {
    GFWFakeAccessory * accessory = [GFWFakeAccessory new];
    DDLogInfo(@"Accessory did disconnect : %@", accessory);
    if(self.connectionBlock){
        self.connectionBlock(accessory);
    }
}

- (BOOL)accessory:(GFWAccessory *)accessory
    isSupportingProtocol:(NSString *)protocolString {
  return YES;
}

- (id <GFWAccessorySession>)accessory:(id <GFWAccessory>)accessory
               openSessionForProtocol:(NSString *)protocolString {
    id <GFWAccessorySession> fakeSession = [GFWFakeAccessorySession new];
    return fakeSession;
}

@end