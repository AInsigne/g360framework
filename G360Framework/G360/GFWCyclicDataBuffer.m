//
// Created by Maxime CHAPELET on 27/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWCyclicDataBuffer.h"


@interface GFWCyclicDataBuffer ()
@property(nonatomic, strong) NSMutableData *buffer;
@property(nonatomic) NSUInteger readIndex;
@property(nonatomic) NSUInteger writeIndex;
@property(nonatomic, readwrite) NSUInteger emptySpace;
@property(nonatomic, readwrite) NSUInteger size;
@property(nonatomic, strong) dispatch_queue_t queue;
@end

@implementation GFWCyclicDataBuffer

- (instancetype)initWithSize:(NSUInteger)size {
    self = [super init];
    if (self) {
        self.buffer = [NSMutableData dataWithLength:size * sizeof(uint8_t)];
        self.readIndex = 0;
        self.writeIndex = 0;
        self.emptySpace = size;
        self.size = size;
        self.queue = dispatch_queue_create("gfwdatabuffer", nil);
    }
    return self;
}

+ (instancetype)createWithSize:(NSUInteger)size {
    GFWCyclicDataBuffer *dataBuffer = [[GFWCyclicDataBuffer alloc] initWithSize:size];
    return dataBuffer;
}

- (BOOL)canWriteDataWithLength:(NSUInteger)length {
    return self.emptySpace >= length
            && self.size >= length;
}

- (BOOL)canReadData {
    return self.emptySpace < self.size;
}

- (NSUInteger)spaceFilled{
    return self.size - self.emptySpace;
}

- (BOOL)writeData:(uint8_t const *)data length:(NSUInteger)length {
    __block BOOL success = NO;
    __weak typeof(self) weakSelf = self;
    if (data) {
        dispatch_sync(self.queue, ^{
            if (![weakSelf canWriteDataWithLength:length])
                return;
            NSUInteger writeIndex = 0;
            for (int i = 0; i < length; ++i) {
                writeIndex = (weakSelf.writeIndex + i) % weakSelf.size;
                uint8_t *dst = weakSelf.buffer.mutableBytes + writeIndex;
                *dst = *(data + i);
            }
            weakSelf.writeIndex = (writeIndex + 1) % weakSelf.size;
            weakSelf.emptySpace -= length;
            success = YES;
        });
    }
    return success;
}

- (BOOL)readByte:(uint8_t *)byte {
    __block BOOL success = NO;
    __weak typeof(self) weakSelf = self;
    if (byte) {
        dispatch_sync(self.queue, ^{
            if (![weakSelf canReadData])
                return;
            uint8_t *const src = weakSelf.buffer.mutableBytes + weakSelf.readIndex;
            *byte = *src;
            weakSelf.readIndex = (weakSelf.readIndex + 1) % weakSelf.size;
            weakSelf.emptySpace++;
            success = YES;
        });
    }
    return success;
}

- (NSInteger)readData:(uint8_t *)buffer length:(size_t)length {
  __block NSInteger readLength = 0;
  __weak typeof(self) weakSelf = self;
  if (buffer) {
    dispatch_sync(self.queue, ^{
      while ([weakSelf canReadData] && readLength < length) {
        uint8_t *const src = weakSelf.buffer.mutableBytes + weakSelf.readIndex;
        buffer[readLength] = *src;
        weakSelf.readIndex = (weakSelf.readIndex + 1) % weakSelf.size;
        weakSelf.emptySpace++;
        readLength++;
      }
    });
  }
  return readLength;
}

@end