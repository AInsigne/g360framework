//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWConstants.h"

NSString *const kExternalAccessoryProtocol = @"kExternalAccessoryProtocol";
NSString *const kAccessoryDisconnectedNotification = @"kAccessoryDisconnectedNotification";

uint8_t const kEndOfStreamPacketSize = 8;
uint8_t const kEndOfStreamPacket[8] = {0x0, 0x0, 0x0, 0x8, 'e', 'o', 's', 'p'};