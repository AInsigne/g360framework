//
//  GFW360camLElementaryStream.h
//  GFWAccessoryKit
//
//  Created by Naz Mariano on 06/09/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWAccessoryNALUsProcessor.h"
#import <GFWAccessoryKit/GFWAccessorySampleBufferProcessor.h>
#import <GFWAccessoryKit/GFWAccessoryKit.h>
#import <EMSLib/Protocols.h>
#import "H264Encoder.h"

@protocol GFW360camLElementaryStreamDelegate <NSObject>
-(void)cameraStreamBecameActive;
@end
@interface GFW360camLElementaryStream : NSObject <GFWAccessoryNALUsProcessor, GFWAccessorySampleBufferProcessor, GFW360camLH264RawNALUsParserDelegate>
-(instancetype)initWithSocket:(id<EncoderReceiver>)aReceiver;
@property (strong, nonatomic) H264Encoder* videoEncoder;
@property (strong, nonatomic) id<GFW360camLElementaryStreamDelegate>delegate;
@end
