//
// Created by Maxime CHAPELET on 23/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWFakeAccessorySession.h"
#import "GFWFakeAccessoryInputStream.h"
#import "GFWFakeAccessory360camLInputStreamBuffer.h"


@interface GFWFakeAccessorySession ()
@property(nonatomic, strong) NSInputStream *fakeInputStream;
@end

@implementation GFWFakeAccessorySession {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.fakeInputStream = [[GFWFakeAccessoryInputStream alloc]
          initWithStreamBuffer:[GFWFakeAccessory360camLInputStreamBuffer new]];
    }

    return self;
}


- (id <GFWAccessory>)accessory {
    return nil;
}

- (NSString *)protocolString {
    return nil;
}

- (NSInputStream *)inputStream {
    return self.fakeInputStream;
}

- (NSOutputStream *)outputStream {
    return nil;
}

@end