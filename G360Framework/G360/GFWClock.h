//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@interface GFWClock: NSObject
- (void)start;
- (void)stop;
- (void)setTime:(CMTime)cmTime;
- (void)getTime:(CMTime *const)cmTime;
@end