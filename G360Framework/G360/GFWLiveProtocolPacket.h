//
//  GFWLiveProtocolPacket.h
//  GFWAccessoryKit
//
//  Created by Maxime CHAPELET on 09/08/2016.
//  Copyright © 2016 Maxime CHAPELET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GFWLiveProtocolPacket : NSObject
@property(nonatomic) unsigned long long offset;
@property(nonatomic) uint32_t type;
@property(nonatomic) uint32_t size;
@property(nonatomic) uint32_t chunk;
@property(nonatomic, strong) NSMutableData *data;
@property(nonatomic) uint64_t timestamp;
@end
