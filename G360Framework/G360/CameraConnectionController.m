//
//  CameraConnectionController.m
//  POC2
//
//  Created by Naz Mariano on 09/09/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//

#import "CameraConnectionController.h"

@implementation CameraConnectionController
+ (instancetype)controller
{
    return [[CameraConnectionController alloc] init];
}

-(void)viewDidLoad {
    NSLog(@"%s", __FUNCTION__);
    id <GFWAccessoryService> service;
    service = [GFWServiceLocator sharedInstance].accessoryService;
    
    weakify(self);
    ConnectionBlock connectedBlock = ^(id <GFWAccessory> accessory) {
        strongify(self);
        self.accessory = accessory;
        self.connected = YES;
        self.connectionBlock(accessory);
        NSLog(@"CameraConnectionController - didConnect: %@, %@", [[accessory protocolStrings] firstObject], accessory);
        
    };

    ConnectionBlock disconnectedBlock = ^(id <GFWAccessory> accessory) {
        strongify(self);
        self.accessory = accessory;
        self.connected = NO;
        self.disconnectionBlock(accessory);
        NSLog(@"CameraConnectionController - didDisConnect");
    };
    
    [service startWithAccessoryConnection:connectedBlock
                            disconnection:disconnectedBlock];
}
@end
