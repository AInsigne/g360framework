//
// Created by Giroptic on 29/07/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import <Foundation/Foundation.h>
@protocol GFWFakeAccessoryInputStreamBuffer <NSObject>
- (void)prefill;
- (void)update;
- (NSUInteger)length;
- (void)getBytes:(uint8_t *)buffer length:(NSUInteger)length;
@end