//
// Created by Giroptic on 01/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFWFakeAccessoryInputStreamBuffer.h"
#import <Foundation/Foundation.h>

@interface GFWFakeAccessory360camLInputStreamBuffer : NSObject <GFWFakeAccessoryInputStreamBuffer>
@end