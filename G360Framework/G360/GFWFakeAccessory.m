//
// Created by Maxime CHAPELET on 22/06/2016.
// Copyright (c) 2016 Maxime CHAPELET. All rights reserved.
//

#import "GFWFakeAccessory.h"

@implementation GFWFakeAccessory {
}
- (BOOL)isConnected {
  return YES;
}

- (NSUInteger)connectionID {
  return 123456789;
}

- (NSString *)name {
  return @"Fake accessory";
}

- (NSString *)manufacturer {
  return @"GIROPTIC Software";
}

- (NSString *)modelNumber {
  return @"1.0.0";
}

- (NSString *)serialNumber {
  return @"123123123123";
}

- (NSString *)firmwareRevision {
  return @"rev A";
}

- (NSString *)hardwareRevision {
  return @"rev 1B2";
}

- (NSArray<NSString *> *)protocolStrings {
  return @[ @"com.giroptic.FakeDevice.stream" ];
}

- (NSString *)description {
  NSMutableString *description;
  description = [NSMutableString
      stringWithFormat:@"<%@ :\n", NSStringFromClass([self class])];
  [description
      appendFormat:@"connectionID: %lu\n", (unsigned long)self.connectionID];
  [description appendFormat:@"name: %@\n", self.name];
  [description appendFormat:@"manufacturer: %@\n", self.manufacturer];
  [description appendFormat:@"modelNumber: %@\n", self.modelNumber];
  [description appendFormat:@"serialNumber: %@\n", self.serialNumber];
  [description appendFormat:@"firmwareRevision: %@\n", self.firmwareRevision];
  [description appendFormat:@"hardwareRevision: %@\n", self.hardwareRevision];
  [description appendFormat:@"protocolStrings: %@\n", self.protocolStrings];
  [description appendString:@">"];

  return description;
}

@end