//
// Created by Giroptic on 04/08/2016.
// Copyright (c) 2016 Giroptic. All rights reserved.
//
#import "GFWPixelBufferQueueElement.h"

@implementation GFWPixelBufferQueueElement {
}

- (instancetype)init {
  self = [super init];
  if (self) {
    _pixelBuffer = NULL;
    _pts = kCMTimeInvalid;
  }

  return self;
}

- (void)setPixelBuffer:(CVPixelBufferRef)pixelBuffer {
  if (_pixelBuffer != pixelBuffer) {
    if (_pixelBuffer) {
      CFRelease(_pixelBuffer);
    }
    if (pixelBuffer) {
      CFRetain(pixelBuffer);
    }
    _pixelBuffer = pixelBuffer;
  }
}

- (void)dealloc {
  if (_pixelBuffer) {
    CFRelease(_pixelBuffer);
    _pixelBuffer = NULL;
  }
}

@end