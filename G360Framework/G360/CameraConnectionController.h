//
//  CameraConnectionController.h
//  POC2
//
//  Created by Naz Mariano on 09/09/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//

#import "ControllerProtocols.h"
#import <GFWAccessoryKit/GFWAccessoryKit.h>
#import "GFWConstants.h"
#import "GFWServiceLocator.h"
#import <Foundation/Foundation.h>

typedef void (^ConnectionBlock)(id<GFWAccessory>);
typedef void (^DisconnectionBlock)(id<GFWAccessory>);

@interface CameraConnectionController : NSObject <ViewLifeCycle>
@property (nonatomic, assign, getter=isConnected) BOOL connected;
@property (nonatomic, strong) ConnectionBlock connectionBlock;
@property (nonatomic, strong) DisconnectionBlock disconnectionBlock;
@property (nonatomic, strong) id<GFWAccessory> accessory;
+ (instancetype)controller;
@end
